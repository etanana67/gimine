# **Įvadas**

Šio darbo akstinu tapo mano šešuro Algirdo Brunono Peškaičio surašyti
atsiminimai apie Peškaičių giminę. Jo sūnus Rėdas vis prisimena, kad
vaikystėje kartu su savo sese Vita dažnai prašydavo tetės, kaip tada
sakydavom, papasakoti apie savo vaikystę. Jiems tai buvo pati įdomiausia
pasaka, o man kaip istorikei visas girdėtas giminės legendas ir
pasakojimus knieti patikrinti. Pradžioje man nesisekė, nes Zarasų rajono
bažnytinių metrikų knygų nebuvo internetinėje svetainėje epaveldas.lt,
kurioje ieškojau informacijos. Netikėta pažintis facebook.com svetainėje
su Mindaugu Baltuška, kuris irgi domėjosi Peškaičių giminę padarė
įmanomu pasigilinti į Daugailių bažnyčios metrikų knygas. Mindaugas
atsiuntė man į el. paštą jo nufotografuotas metrikų knygas, o jo
kvietimas į facebook uždarą grupę "Domiuosi genealogija" padėjo atrasti
kitus šaltinius esančius internete, tokius kaip familysearch.org ir
genmetrika.eu. Informacijos ieškojau ir išleistose knygose, kuriose yra
paminėta Peškaičių pavardė, žinoma nesu tikra, kad peržiūrėjau viską.

Rasti dokumentai šiuose šaltiniuose padėjo ne tik patvirtinti Algirdo
Peškaičio pasakojimus, bet ir juos papildyti. Dar liko daug neaiškumų ir
netikslumų, nes dar negaliu peržiūrėti Dusetų, Antazavės ir Baltriškės
bažnyčių metrikų knygas, o taip pat Rusijoje esančių dokumentų, kurių
internetinėje erdvėje dar mažiau nei Lietuvos. Šiose vietovėse gyveno
Peškaičiai, o Rusijoje gyveno Mykolas Peškaitis su šeima (Algirdo
Peškaičio senelis). 2020 metais kilusi corona viruso pandemija netikėtai
atvėrė laisvą priėjimą prie Sankt Peterburgo suskaitmenintų archyvų,
kuriuose suradau Mykolo ir Emilijos Peškaičių trijų sūnų Vladislovo,
Jono ir Boleslovo gimimo liudijimus. Bet liko neaišku, kur gimė ir buvo
pakrikštyti kiti vaikai?

Čia sudėjau viską ką man pavyko surasti apie Peškaičių giminę, net jeigu
tai atrodo, kad tiesiogiai nėra susiję su mūsų giminės atšaka, gal vėliau atsiras
galimybė susipažinti su dabar nepasiekiamais dokumentais ir bus galimybė
išsiaiškinti gal kažkokia giminystė vis tik yra.

## Peškaičiai

Lietuvoje yra vietovė turinti tiesioginių sąsajų su Peškaičių giminę,
tai Utenos rajone Dusetų seniūnijoje esantis Peškaitiškiu kaimas.
Greičiausiai ten kažkada gyveno Peškaičių giminės bočiai. Daug pavardžių
Lietuvoje yra kilusios nuo gyvenviečių pavadinimų ir atvirkščiai
gyvenviečių pavadinimas kilęs nuo gyvenusių ten žmonių pavardės.
Ankstesniais laikais buvo įprasta sakyti: Jonas vilniškis ar koks Juozas
kauniškis, o naujai kuriamai gyvenvietei suteikdavo steigėjo pavadinimą.
Taigi kaip buvo su pavardę Peškaitis nėra aišku, bet sprendžiant iš
kaimo pavadinimo Peškaitiškės žodžio sandaros, panašu, kad kaimas buvo
pavadintas Peškaičių garbei.

1962 metais išleistoje knygoje „Lietuvos inventoriai XVII a." 1675 m.
balandžio 24 d. Venių dvaro ir Kroviškių palivarko, Ukmergės pavieto,
inventoriuje yra minimi Peškaičiai, kurie gyveno Landiškės kaime ir
Antalieptės miestelyje.[^1] Dvaras buvo 8 km į rytus nuo Antalieptės,
kelių Degučiai -- Dusetos, Antalieptė -- Zarasai kryžkelėje, 226,8 ha
Vencavo ežero pietiniame krante. Vencavų kaimo pavadinimą prof. S.
Kolupaila kildino nuo iškreipto romėnų deivės Veneros vardo. Baltarusių,
lenkų ir rusų kalbomis rašytuose dvaro dokumentuose buvo rašoma: Venus,
Venusov, Venusovo, o vietovė pradėta minėti nuo XVII a. pradžios. Dvarui
priklausė daug kaimų, palivarkų ir ežerų.[^2] 1675 metų inventoriuje be
Vencavų dvaro surašyti šie kaimai: Rowiany, Beedzie, Jakunce
(Jakunčiai), Gaydele (Gaideliai), Zacharkowie boierowie (Zokoriai),
Tatyrny (Tetervos), Swidze (Svidžiai), Podymnicy, Lundziszki
(Landiškės), Zwielbucia (Žvilbučiai), Kuykie (Kuikiai), Jodkunowie
(Jatkūniškės), Mozwiele. Gaila, bet dvaro pastatai sunaikinti
sovietmečiu.

![](./media/image124.png){width="3.9479166666666665in"
height="2.7708333333333335in"}

![Landiškių kaimo aprašas](./media/image92.png) Landiškių kaimas

{width="3.7284722222222224in"
height="0.78125in"}

Valakai du -- vienas apgyvendintas, kitas tuščias

Puse valako Kazimieras Peškaitis, apgyvendintas, brolis Povilas,
Baltrus, sūnus Motiejus, du arkliai, jaučių žagrė; puse valako ten pat
tušti už činšą, viena lietuviška kapa; puse valako Kasparas Peškaitis,
apgyvendinta, jaučių žagrė; taip pat tuščia už činšą vieną lietuvišką
kapą.

Šio kaimo prievolės tokios pat kaip Beedziow (nerandu šio kaimo, gal
išnykęs) ir kitų toks pats mokestis.

Antalieptės miestelyje daržininkas/sodininkas.

Jonas Peškaitis, apgyvendintas, darže/sode, broliai su juo du -- Jurgis
ir Lukas, sūnūs -- Kazimieras ir Motiejus, šeši arkliai, trys jaučių
žagrės; ten pat saugo daržą tuščią, tarnauja vasarą tris dienas su
pinigais, białogłowa[^3] (verčiasi kaip ištekėjusi moteris) ketvirtą,
žiemą tris dienas, už verpalus duoda linų pusę šimto, už apgyvendintą
moką ir tuščią su kruopom pusketvirtos kapos, linų, kurio duodą, turi
sverti 10 svarų tam, kad verpti neina."

Kaip matome anksčiau ūkininkaudavo kartu net tik su savo sūnumis, bet ir
su broliais. Tokiu būdu buvo siekiama nesmulkinti ūkių. Paprastai
vyriausias sūnus paveldėdavo ūkį, o kiti broliai jam padėdavo
ūkininkauti arba eidavo tarnauti pas kitus ūkininkus, arba užkuriais į
žmonos ūkį. Jeigu brolių nebuvo, tai ūkyje dirbdavo samdiniai. Samdiniai
taip pat būdavo surašomi reviziniuose ir inventoriniuose sąrašuose,
kartu su ūkių šeimininkais baudžiauninkais, kaip matome inventorinio
sąrašo Peškaičiams brolių ir sūnų netrūko.

**Daugailiai.** Žvilgtelėkime į Daugailių bažnyčios istoriją, kurios metrikų knygose įrašytas Peškaičių giminės XIX a. pab. - XX a. pr. svarbiausi gyvenimo įvykiai: gimimas, santuoka, mirtis\...

Nuotraukoje Daugailių bažnyčia 1916
m.![](./media/image48.png){width="5.0322922134733155in"
height="3.5385793963254595in"}

Pirmoji Daugailių bažnyčia pradėjo veikti 1685 m., ir nuo to laiko
pradėtos rašyti metrikų knygos. 1744 m. kita bažnyčia buvo pastatyta ant
Daugailių piliakalnio, ji buvo Dusetų bažnyčios filija, 1766 m. sudegė,
į ją trenkus žaibui. Ją aptarnavo vienuolis pranciškonas. 1852 m.
pastatyta nauja. Dusetų klebono Antano Rumševičiaus rūpesčiu 1883 m.
bažnyčia perstatyta ir padidinta (miško medžiagos paaukojo Adomas
Kviklys). Daugailiuose 1897--1935 m. klebonavo Kazimieras Kiršinas
(1865--1940). Jis rūpinosi parapijiečių švietimu, globojo iš parapijos
kilusius klierikus, 1915 m. suremontavo bažnyčią. Palaidotas Daugailių
kapinėse. Kunigo Kazimiero Mozūro iniciatyva 1939 m. įkurta parapija,
nudažyta bažnyčia. 1940 m. išdažytas jos vidus, pastatyta medinė
klebonija. Klebono Petro Baltuškos (Daugailiuose nuo 1970 m.) rūpesčiu
bažnyčia 1982 m. suremontuota ir perdažyta. Bažnyčia liaudies
architektūros formų, turi baroko bruožų, kryžminio plano, dvibokštė.
Apsidė penkiasienė. Vidus 3 navų. Yra 3 altoriai. Šventoriaus tvora
akmenų mūro. Jo kampe stovi medinė
varpinė.[^4]![](./media/image35.png){width="4.0635422134733155in"
height="3.050068897637795in"}

1915 m. Daugailių bažnyčia vokiečių buvo paversta karo ligonine.[^5]

![](./media/image22.jpg){width="6.0322922134733155in"
height="3.9496030183727036in"}

Įrašai seniausiuose Daugailių bažnyčios metrikų knygose, patvirtina -
Peškaičių giminė nuo seno gyveno Daugailių apylinkėse. Jau pirmajame
1686 -- 1726 m. krikšto metrikų knygos[^6] puslapyje du kartus įrašyta
Peškaičių pavardė. Nors krikšto įrašai rašyti lotynų kalba, bet pavardės
ir gyvenviečių pavadinimai rašyti lenkiškai, nors ir su lietuviška
galūne: Pieszkaytis, Peszkaytys, Daugiele (Daugailiai), Zacharka
(Zakarka), Zachary (Zokoriai).

Norėčiau pasakyti kokia giminystė dabartinius Peškaičius sieja su šiuose dokumentuose įrašytais Peškaičiais, tačiau negaliu to padaryti, nes internete kol kas nėra visų Daugailių ir Dusetų bažnyčių knygų. Tikiuosi ateityje bus galima atsakyti į šį klausimą. Aišku tik viena - Peškaičiai šiame krašte gyveno jau XVII amžiaus pabaigoje.

1686 m. Daugailių bažnyčios krikšto knygos dalis pirmo
puslapio.![](./media/image14.png){width="6.6930555555555555in"
height="2.9965277777777777in"}

Dalis antro puslapio:
---------------------

![](./media/image33.png){width="5.4187007874015745in"
height="4.61444116360455in"}

**Murliškės.** Šalia Kauno -- Zarasų plento 24 km. nuo Zarasų, ant
kalvelės, gerai matomas iš tolo, išsidėstė Murliškių palivarkas, XIX a.
pabaigoje priklausęs *Amirai Jablonskai*, jai mirus -- *Liucijui
Jablonskiui*. Murliškėse buvo dvaro smuklė, kuri XIX a. ribose priklausė
*Jablonskiams* ir *Bliumams*.

Caro valdymo laikais *Jablonskis* šį pastatą išnuomojo žydui
(*Bliumui?*), kuris jame įkūrė ne tik smuklę, bet ir suteikdavo nakvynę
pravažiuojantiems. Viename pastate buvo smuklė (karčema), kitame --
tvartas, ratinė su daržine ir svirnas.

Smuklę-*užvažiuojamąjį kiemą* sudarė du lygiagretūs 5 m. atstumu
stovintys pastatai, kuriuos suprojektavo Vilniaus gubernijos architektas *Ivanas Karasinskis.* 1840 m. juos pastatė Vilniaus gubernijos
architektas *Kazimieras Fronckevičius*.

Karčemos pastatas vieno aukšto, stačiakampio plano, 40x9 metrų dydžio,
sumūrytas iš raudonų plytų, tinkuotas ir nubaltintas. Stogas dvišlaitis,
dengtas malksnomis. Pastato galines sienas remia masyvūs akmenų
*kontraforsai*. Fasadai kuklių formų, suskaidyti langais. Tik pietinio
fasado viduryje yra stoginis prieangis (*„gonkas"*) su *frontonu*. Iš
rytų ir vakarų pusių kiemą supa iš lauko akmenų sumūryta aukšta tvora,
kuri pratęsia galines karčemos sienas ir ištisai uždaro vakarinę ir
rytinę kiemo dalis. Abiejuose kiemo galuose buvo sunkūs, aklini vartai.
Smuklės rytinės pusės šiauriniame gale yra nedidelis rūsys su tašytų
akmenų laiptais. Rūsį dengia cilindrinis plytų mūro skliautas su
siaurais grotuotais langeliais. Kieme prie rytinės tvoros glaudžiasi
pusiau medinis ūkinis pastatas 29x9 metrų dydžio, kuriame buvo tvartas,
ratinė su daržine ir svirnas.

Apylinkės ūkininkai šia smukle nesidomėjo, ją dažniausiai lankydavo
vietiniai sentikiai. Caro laikais smuklė ir jos teritorija buvo
savotiška vogtų arklių slėpimo stotimi. Būdavo, pavagia burliokas
ūkininko arklį, šis arklį vejasi, suranda pėdsaką, bet ties smukle
arklys dingsta kaip ugnyje. Visas šias vagystes globojo carinės Rusijos
policija. Pagal senus padavimus čia ir lietuvių nemažai pėdsakų
dingę\...

Iš tūlo *Vinco Tamošiūno* užrašytų prisiminimų, kas dėjosi anuo met
peržengus Murliškių smuklės slenkstį: „Kadaise, labai seniai, Davainių
*Martynas Barisa* nusibeldęs į Murliškių smuklę ir ten taip nusigėręs,
jog kelio į namus jau neberadęs. Beklaidžiodamas jis atsidūrė Šlepečių
galilaukėje esančiame aukštame kalne ir ten buvęs „degtinės suplėšytas į
gabalus". Visa tai stebėjęs kiškis. Kai jau Martynas miręs, kiškis
pasiėmęs gabalą raudonų marškinių ir juo prisidengęs. Tas kiškis buvęs
girtuoklio Martyno siela, neturėjusi vietos žemėje\...".

Šiandien iš kažkada liūdnai pagarsėjusios Murliškių smuklės liko tik
praeities legendos, medžiais ir krūmais apaugusių sienų griuvėsiai,
architektūrinių fragmentų nuolaužų krūvos. Tokio unikalaus
architektūriniu išplanavimu komplekso-karčemos ir ūkinio pastato
jungties analogo Lietuvoje jau neberasime.[^7]

**Peškaičiai XIX - XX a.**
--------------------------

Nuo 1871 metų Peškaičiai, iš kurių kilusi mūsų giminės atšaka, gyveno
kaime šiuo metu vadinamame Mitrauka. Šio kaimo pavadinimas XIX a.
pabaigos metrikų įrašuose įvairavo: Trakelių, Dimitraukos ir Mitraukos
užusenis[^8]. Kodėl tą pačią vietovę ir tuo pačiu metu vadino
skirtingais pavadinimais sunku pasakyti, gal buvo naujai įsikūrusi ir
pavadinimas dar nebuvo nusistovėjęs? Šioje vietovėje 1883 kovo 11 dieną
iš Murliškių palivarko ponios Jablonskajos broliai Benediktas ir
Nikodemas Peškaičiai nusipirko 6,158 dešimtinės žemės, o Adomas ir
Ignotas Zakarkos -- 7,1044 dešimtinės[^9]. Iš Benedikto ir Nikodemo
santuokos liudijimų sužinome, kad jų tėvai Mykolas Peškaitis ir Marijona
Zakarkaitė, kaip įrašyta metrikose - Zacharkova. Taigi ar nebus Adomas
ir Ignotas Zakarkos, kokie pusbroliai Benediktui ir Nikodemui
Peškaičiams?

Vieta Mitraukoje kur stovėjo Peškaičių sodyba, dabar ten liko tik
šulinys. 2019 m.

![](./media/image73.jpg){width="5.1363527996500435in"
height="3.8614577865266844in"}

Mitraukos sodyba, kurioje Zakarkų giminė gyveno daugiau nei 100 metų,
šiuo metu nebegyvenama 2019 m.

> ![](./media/image122.jpg){width="5.0625in"
> height="3.8059372265966753in"}

Gilinantis į giminės istoriją iškyla nemažai klausimų į kuriuos deja jau
niekas nebe atsakys. Kiekviena giminė turi savo pasakojimų, legendų ir
ne visada tos legendos atitinka tikrovę. Viena iš Peškaičių giminės
legendų, kad Mykolo Peškaičio tėvas Nikodemas tarnavo arklininkų pas
dvarininką ir kad jis buvo laisvasis valstietis. Mano rastas 1858 metų
Inventorinis Zokorių kaimo sąrašas šią legendą paneigia, bet tuo pačiu
iškelia kitų klausimų.

![](./media/image152.png){width="6.6930555555555555in"
height="4.347916666666666in"}

"Revizinis sąrašas

1858 05 22 Kauno gubernijos Vilkmergės (Ukmergės) apskrities Užpalių
parapijos Zokorių kaimo Dvarininkės Reginos Lenkševičevos apie esančius
jame \[sąraše\] vyriškos ir moteriškos lyties baudžiavinius
valstiečius."[^10]

Iš šio dokumento sužinome, kad Peškaičiai vis tik buvo baudžiauninkai,
dar įdomu, tai kad broliai Benediktas ir Nikodemas bent jau nuo 1855
metų gyveno atskirai ir dar Zokorių kaimas priklausė Užpalių parapijai.

Benediktas Peškaitis 1855 metais buvo 25 metų amžiaus ir yra įrašytas
vienas atskirame kieme, kuris 9 surašymo metu buvo 8, o 10 surašymo metu
1858 metais - 4. Iš kitų įrašų matyti, kad Zokoriuose per tris metus
gerokai sumažėjo kiemų ir gyventojų juose. Paieškojus literatūros šia
tema radau Leono Mulevičiaus knygą „Kaimas ir dvaras Lietuvoje XIX
amžiuje", kurioje buvo atsakymai į mane dominančius klausimus ir faktai
apie kuriuos nebuvau girdėjusi, pavyzdžiui, kad 1864 metais baudžiavos
panaikinimo manifestas buvo išverstas į lietuvių kalbą tikintis taip
sumažinti valstiečių pasipriešinimą, nes Lietuvoje dauguma valstiečių
visiškai nesuprato rusų kalbos arba kad reformos pradžioje nebuvo
atsižvelgta į agrarinių santykių ypatybes buvusias Lietuvoje ir
Baltarusijoje.[^11] Kaip pastebi autorius: „Lietuvoje buvo ne
bendruomeninis, kieminis dirbamos žemės valdymas. Kiekvienas valstiečių
ūkis pats buvo atsakingas už pagrindines prievoles dvarui. Tačiau kol
buvo rėžių sistema su priverstine sėjomaina ir bendrai naudojamomis
ganyklomis, tol egzistavo kaimo bendruomenė. Valstiečius siejo privalomi
vienalaikiai žemės darbai, bendras ganymas, bendruomenės (komunalinės)
kasos, javų magazinai, kelių priežiūra, nepajėgiu bendruomenės narių
globa, viešosios tvarkos saugojimas ir t.t."[^12]

Paskaičius knygą pasidarė aišku kodėl įvyko toks staigus gyventojų
sumažėjimas Zokoriuose. Dvarininkai žinodami apie ruošiamą reformą,
kurią įgyvendinant buvo numatytas baudžiavos panaikinimas, siekė naudos.
„Vos tik pradėjus rengti reformą (1857), dvarininkai masiškai varė
valstiečius nuo žemės ir geresnę žemę keitė į blogesnę."[^13]
Reviziniame sąraše matosi, kad prieš tris metus 1855 m. buvo bent jau 15
kiemų, o 1858 metais surašyti tik 4 ir juose liko tik 5 vyrai. Vadinasi
visi kiti valstiečiai buvo perkelti kitur, o jų žemė prijungta prie
dvaro žemių.

„Iki reformos Lietuvos valstiečiai nuo seno žeme naudojosi kiemais. Jie
atliko prievoles atsižvelgiant ne į „sielų" skaičių, bet į žemės kiekį
bei rūšį. Pagalvės mokestį ir pinigines rinkliavas paskirstydavo
dvarininkas, dažnai nesilaikydamas „sielų: apmokestinimo principo.
Valstiečiai neturėjo bendruomenės, todėl žemė nebuvo perskirstoma. Žemės
sklypus perduodavo paveldėjimo keliu, kartais, jeigu paveldėtojai ūkius
pasidalindavo, buvo surašomi testamentai. Tačiau dvarininkams buvo
naudingiau išlaikyti neišskirstytus ūkius, ypač jeigu valstiečiai mokėjo
činšą (piniginę duoklę)."[^14]

Šiame revizineme sąraše kažkodėl neįrašyta Benedikto žmona, nors 1856
metais jis vedė Dusetų bažnyčioje, greičiausiai darant įrašą, jos
neįrašė. Jo brolis Nikodemas 1855 metais būdamas 15 metų gyveno kartu su
Mykolo Zakarkos šeima, matyt jo mamos Marijonos Zakarkaitės
giminaičiais, atsižvelgiant į Mykolo Zakarkos amžių galėjo būti Nikodemo
pusbroliu. 1857 metais, jei gerai supratau Nikodemas buvo perkeltas į
Gaidelius. Artimiausi Gaideliai yra netoli Antalieptės, tačiau negaliu
būti tikra, kol neradau tai patvirtinančių dokumentų. Bet gali būti, kad
ten įrašyta - бежал, o tai reiškia pabėgo. 1862 metų santuokos liudijime
įrašyta, kad Nikodemas Peškaitis buvo iš Pašekšės kaimo.

![](./media/image97.png){width="3.8864588801399824in"
height="1.4955489938757656in"}

(Atskiru dokumentu 1857 m. perkelti iš Gaidelių dvaro.)

Tai kad Nikodemas būdamas 15 metų gyveno su giminėmis, o Benediktas
kieme įrašytas vienas rodo jog jų tėvai greičiausiai jau mirę. Visa ši
informacija atskleidžia koks sunkus buvo brolių gyvenimas ir galime tik
žavėtis Peškaičių darbštumu, juk ne kiekvienas buvęs baudžiauninkas
sugebėjo uždirbti tiek, kad nusipirktų žemės!

Šis dokumentas iškėlė kelis naujus klausimus:

1. Kodėl broliai gyveno atskirai?

2. Jeigu brolių tėvai mirę, tai kada ir kur?

3. Kodėl neįrašyta Benedikto žmona?

4. Jeigu 1857 m. Nikodemas perkeltas ar pabėgo, tai kur ir kada jis
   
   > persikraustė į Pašekšės kaimą?

5. Kodėl Zokoriuose atskirame kieme gyvenęs Benediktas vėliau su šeima blaškosi po "svietą" ir kam atiteko to kiemo valda?

6. Kodėl nuo 1871 m. kartu su broliu Nikodemu apsigyvena kartu (viename kieme) Mitraukoje?

![](./media/image155.png)

Revizijos sąrašas

1858 05 22 Kauno gubernijos Ukmergės apskrities Užpalių parapijos
Zokorių kaimo

| a   | b   | c   |
|:--- |:---:| ---:|
| d   | e   | f   |
| g   | h   | i   |
| j   | k   | l   |

+----------+----------+----------+----------+----------+------+
| Šeima    | Vyriškos | Pa       | Iš jų    | Kiek     |      |
|          | lyties   | skutinės | išvyko   | dabar    |      |
|          |          | r        |          |          |      |
|          |          | evizijos |          |          |      |
|          |          | metų     |          |          |      |
|          |          | buvo ir  |          |          |      |
|          |          | po jos   |          |          |      |
|          |          | atvyko   |          |          |      |
+==========+==========+==========+==========+==========+======+
| Revizija | Revizija | Zokorių  | Metų     | Kada     | Metų |
| Nr. 9    | Nr. 10   | kaimas   |          | būtent   |      |
|          |          | val      |          |          |      |
|          |          | stiečiai |          |          |      |
+----------+----------+----------+----------+----------+------+
| 4\.      | 1\.      | Mykolas  | 28       | „        | 31   |
|          |          | Justino  |          |          |      |
|          |          | s.       | 15       | pabėgo   | „    |
|          |          | Zakarka  |          | 1857     |      |
|          |          |          |          |          |      |
|          |          | N        |          |          |      |
|          |          | ikodemas |          |          |      |
|          |          | s.       |          |          |      |
|          |          | Mykolo   |          |          |      |
|          |          | P        |          |          |      |
|          |          | eškaitis |          |          |      |
+----------+----------+----------+----------+----------+------+
| 8\.      | 4\.      | Be       | 25       | „        | 28   |
|          |          | nediktas |          |          |      |
|          |          | s.       |          |          |      |
|          |          | Mykolo   |          |          |      |
|          |          | P        |          |          |      |
|          |          | eškaitis |          |          |      |
+----------+----------+----------+----------+----------+------+

![](./media/image153.png){width="6.6930555555555555in"
height="6.723611111111111in"}

+---------------+---------------+---------------+------------+------+
| Šeima         | Moteriškos    | Laikinai      | Kiek dabar |      |
|               | lyties        | išvyko        |            |      |
+===============+===============+===============+============+======+
| Revizija Nr.  | Revizija Nr.  | Zokorių       | Nuo kada   | Metų |
| 9             | 10            | kaimas        |            |      |
|               |               | valstiečiai   |            |      |
+---------------+---------------+---------------+------------+------+
|               |               | Mykolo        | „          | 28   |
|               |               | Justino s.    |            |      |
|               |               | žmona         | „          | 8    |
|               |               | Liudvika      |            |      |
|               |               |               | „          | 24   |
|               |               | Jo duktė      |            |      |
|               |               | Grasilė       |            |      |
|               |               |               |            |      |
|               |               | Jo sesuo      |            |      |
|               |               | Uršulė        |            |      |
+---------------+---------------+---------------+------------+------+

Kol kas neaišku kiek vaikų turėjo Mykolas Peškaitis su Marijona
Zakarkaite. Vyriausias iš mums žinomu dviejų brolių -- Benediktas, jis
net 10 metų vyresnis už brolį Nikodemą.

**Benediktas Peškaitis 1824/1830 -- 1883 08 15**

**Agota Šileikytė 1837/1841 -- 1884 11 15**

1856 vasario 26 dieną Dusetų bažnyčioje Benediktas Peškaitis, 26 metų
jaunuolis susituokė su Agota Šileikyte, 19 metų mergina, Andriaus
Šileikio ir Marijonos Šepetytės dukra. Abu valstiečiai iš Dusetų
parapijos. Santuokos liudininkai: Matas Zakarka, Antanas Jaškutis. Po
santuokos jaunavedžiai, ką patvirtiną 1858 metų inventorinis sąrašas
gyveno Zokorių kaime, kiek ilgai pasakyti sunku, bet 1863 metais
Daugailių bažnyčioje pakrikštyta jų duktė Marijona ir jos gimimo vieta
-- Drąsėnų kaimas, Daugailių valsčiuje. Kitų vaikų krikšto įrašai
Daugailių bažnyčios krikšto metrikų knygose atskleidžia tuometinę
Benedikto šeimos gyvenamąją vietą: 1865 metai dar Drąsėnų kaime, 1867
metais -- Murliškių užusienis, o nuo 1871 metų -- Mitraukos užusienis,
taigi čia apsigyveno dar prieš tai, kai su broliu nusipirko žemės.
Žinant, kad abu broliai po žemės pirkimo greitai pasimirė, visi abiejų
brolių veiksmai atsiskleidžia kiek kitu kampu. Kadangi abu broliai sirgo
džiova, tai jų artėjanti mirtis niekam nebuvo paslaptis ar netikėtumas.
Vadinasi, tai kad jie matyt bendrai ūkininkavo ir kartų pirko žemes,
buvo sąmoningas žingsnis, mąstant apie paliekamų vaikų ateitį.
Greičiausiai tarp brolių buvęs susitarimas, kad Nikodemas bendrai su
Benediktu perka žemes, o Benedikto sūnus Mykolas, kuriam atiteks ūkis
pasirūpina ir duoda dalį Nikodemo vaikams. Nikodemas mirdamas paliko
tris mažamečius vaikus Karolį, Agotą ir Mykolą, kad bus dar vienas tuo
metu greičiausiai nė nežinojo. Anelė gimė praėjus 8 mėnesiams po jos
tėvo mirties. Taigi mirus Nikodemui ir Benediktui ūkis atiteko Benedikto
sūnui Mykolui.

Visuose įrašuose socialinė Benedikto ir jo tėvų padėtis įvardijama kaip
valstiečių. Benedikto ir Agotos santuoka truko 26 metus ir mirdami jie
paliko keturis sūnūs ir dvi dukras. Iki galo neaišku kada gimė
Benediktas: santuokos liudijime įrašyta, kad jam buvo 26 metai, tai tada
jis turėjo gimti 1830 m., o mirties liudijime įrašyta, kad jis mirė 59
metų, tuomet gimimo metai turėtų būti 1824 m. Taip pat nėra aiški ir
Agotos gimimo data: santuokos įraše, jai 19 metų, tai gimimo metai -
1837, o mirties liudijime įrašyta, kad mirė būdama 43 metų amžiaus,
gaunasi, kad gimė 1841 m. ir tekėdama turėjo būti tik 15 metų. Kol
neradau jos gimimo liudijimo negalima būti tikriems, kad taip ir buvo.

Benediktas Peškaitis mirė Mitraukoje 1883 rugpjūčio 15 dieną, nuo
tuberkuliozės, kaip įrašyta mirties metrikų knygoje būdamas 59 metų
amžiaus. Po savęs paliko žmoną Agotą gim. Šileikyte, vaikus: Mykolą
(apie 27 m.), Antaną (apie 25 m.), Benediktą (17 m.), Klemensą (15 m.),
Marijoną (20 m.) ir Grasildą (12 m.). Agota Peškaitienė neilgai našlavo
ir 1884 lapkričio 15 dieną mirė Trakeliuose, būdama 43 metų amžiaus taip
pat nuo tuberkuliozės. Abu palaidoti Daugailių kapinėse.

Benedikto Peškaičio mirties įrašas Daugailių bažnyčios metrikos
knygoje:![](./media/image71.jpg){width="6.532638888888889in"
height="2.5569444444444445in"}

Benedikto Peškaičio santuokos įrašas Dusetų bažnyčios metrikos
knygoje:![](./media/image103.jpg){width="6.434027777777778in"
height="4.885416666666667in"}

Agotos Peškaitienės mirties įrašas Daugailių bažnyčios metrikos knygoje:

![](./media/image135.jpg){width="6.6930555555555555in"
height="2.704861111111111in"}

Kiek iš viso Benediktas su Agota turėjo vaikų neaišku iš rastų dokumentų
žinoma apie šiuos:

**1.** **Antanas Peškaitis** gimęs apie 1858 metus. 1886 lapkričio 16
dieną Daugailių bažnyčioje susituokė 28 metų jaunuolis iš Krevišų(?)
užusienio, Dusetų parapijos su bajoraite **Zofija Steponavičiūte** (gim.
apie 1866 m.) 19 metų mergina iš Zokorių kaimo, Daugailių parapijos,
bajorų Romualdo Steponavičiaus ir Onos Saulevičiūtės dukra. Liudininkai:
Mykolas Peškaitis, greičiausiai Antano brolis, Ignotas Maračinskas?,
Adomas V. Kadangi Antanas gyveno Dusetų parapijoje, tai jaunoji po
santuokos persikėlė gyventi į vyro namus Dusetų valsčiuje. Kaip
susiklostė tolesnis likimas neaišku.

Antano Peškaičio, Benedikto sūnaus, santuokos įrašas Daugailių bažnyčios
metrikos knygoje:

![](./media/image147.jpg){width="6.6930555555555555in"
height="6.211805555555555in"}

**2. Marijona Peškaitytė Mikulionienė** gimė 1863 vasario 19 dieną,
Drąsėnų kaime, pakrikštyta Daugailių bažnyčioje, krikštatėviai Adomas
Blažėnas su Prakseda Zakarkaite.

**1886 birželio 4 dieną** Daugailių bažnyčioje susituokė **Izidorius
Mikulionis**, 35 metų jaunuolis iš Šaltamalkių? kaimo Dusetų valsčiaus
ir Dusetų bendruomenės, valstiečių Lauryno Mikulionio ir Barboros
Dečiūnaitės(?) sūnus su valstiete Marijona Peškaityte 23 metų mergina iš
Drąsėnų kaimo, Daugailių parapijos. Liudininkai: Juozapas Šepetys,
Fabijonas Alaunė?, Zakarijus Namajūnas. Kaip susiklostė tolesnis
Marijonos likimas nežinia.

Marijonos Peškaitytės krikšto įrašas Daugailių bažnyčios metrikos
knygoje:

![](./media/image143.png){width="6.6930555555555555in"
height="0.9027777777777778in"}

![](./media/image141.png){width="6.6930555555555555in"
height="1.073611111111111in"}

Marijonos Peškaitytės santuokos įrašas Daugailių bažnyčios metrikos
knygoje:

![](./media/image105.png){width="6.178125546806649in"
height="6.018945756780402in"}

**3. Benediktas Peškaitis** gimė 1865 rugsėjo 17 dieną Drąsėnų kaime,
pakrikštytas Daugailių bažnyčioje, krikštatėviai Jonas Zakarka su Julija
Nosteraite. Daugiau nieko nepavyko rasti.

Benedikto Peškaičio krikšto įrašas Daugailių bažnyčios metrikos knygoje:

![](./media/image94.png){width="6.6930555555555555in"
height="1.0881944444444445in"}![](./media/image16.png){width="6.6930555555555555in"
height="1.4166666666666667in"}

**4. Klemensas Peškaitis** gimė 1867 lapkričio 23 dieną Murliškių
užusienyje, pakrikštytas Daugailių bažnyčioje, krikštatėviai Jonas
Zakarka su Grasilda Nikodemo Peškaičio žmona.

Klemenso Peškaičio krikšto įrašas Daugailių bažnyčios metrikos
knygoje:![](./media/image46.png){width="6.6930555555555555in"
height="1.5569444444444445in"}

![](./media/image58.png){width="6.627777777777778in"
height="1.6263888888888889in"}

**5. Grasilda Peškaitytė** gimė 1871 balandžio 20 dieną Mitraukos
užusienyje, pakrikštyta Daugailių bažnyčioje krikštatėviai Nikodemas
Peškaitis su Marijona Bonifacijaus Lamanausko žmona.

Grasildos Peškaitytės krikšto įrašas Daugailių bažnyčios metrikos
knygoje:

![](./media/image109.png){width="6.729166666666667in"
height="1.198611111111111in"}![](./media/image108.png){width="6.563194444444444in"
height="1.1659722222222222in"}

**6. Agota Peškaitytė** gimė 1873 rugsėjo 7 diena Dimitraukos
užusienyje, pakrikštyta Daugailių bažnyčioje, krikštatėviai Jurgis
Lamanauskas su Uršule Zakarkaite. Mirė Murliškėse 1875 sausio 24 dieną
nuo laringito 2 metukų amžiaus. Palaidota Dulių kaimo kapinėse.

Agotos Peškaitytės krikšto įrašas Daugailių bažnyčios metrikos knygoje:

![](./media/image66.png){width="6.6930555555555555in"
height="0.8145833333333333in"}![](./media/image31.png){width="6.6930555555555555in"
height="0.8465277777777778in"}

Agotos Peškaitytės mirties įrašas Daugailių bažnyčios metrikos knygoje:\
![](./media/image118.png){width="6.6930555555555555in"
height="1.3618055555555555in"}

**7. Mykolas Peškaitis** (gim. apie 1856 m.) Mirus dėdei Nikodemui ir
abiems tėvams Mykolui su dėdiene Grasilda teko rūpintis ūkiu ir
likusiais abiejų šeimų vaikais. Praėjus šešeriems metams po Mykolo mamos
mirties **1890 lapkričio 11 dieną** Daugailių bažnyčioje vedė **Salomėją
Žemaitytę**, valstiečių Antano Žemaičio ir Domicelės Gutėnaitės dukterį.
Panašu, kad Mykolas buvo vyriausias Benedikto ir Agotos sūnus, nes
būtent jam atiteko ūkis Mitraukoje, kas atsispindi ir 1923 metų surašymo
duomenyse: Mitraukoje stovėjo 4 sodybos su 29 gyventojais, tai Bernardo
ir Norberto Zakarkų, Alfonso ir Mykolo Peškaičių šeimos[^15].

Mykolo Peškaičio, Benedikto sūnaus, santuokos įrašas Daugailių bažnyčios
metrikos knygoje:

![](./media/image134.jpg){width="6.6930555555555555in"
height="5.475694444444445in"}

Mykolas su žmona Salomėja turėjo keturis vaikus:

1. **Kiprijonas Peškaitis** gimė 1891 m. rugsėjo 13 dieną Trakeliuose,
   
   > pakrikštytas Daugailių bažnyčioje, krikštatėviai Rapolas
   > Seniauskas su Ona Žemaityte. Spėju, kad buvo vadinamas Jonu, jeigu
   > taip, tai jis su sese Leokadija apie 1928 metus išvyko į JAV ir
   > gyveno Čikagoje.

Kiprijono Peškaičio krikšto įrašas Daugailių bažnyčios metrikos
knygoje:![](./media/image62.png){width="6.649305555555555in"
height="2.563888888888889in"}

> JAV 1930 metų visuotiniame gyventojų surašyme yra įrašytas Jonas
> Peskaitis ir jo žmona Petronėlė. Emigravimo metai nurodyti 1912 m., o
> gimimo metai 1881 m., todėl kelia abejonių ar tikrai, tai mus
> dominantis asmuo. Jei vis dėlto, tai tas Jonas tik dėl kažkokių
> aplinkybių pasisendines 10 metų, o gal buvo padaryta klaida įrašant,
> tai čia dar pateikiama kita įdomi informacija: gyveno nuosavame name,
> kurio vertė 5000 dolerių, turėjo radiją (matyt tuo metu, tai buvo
> svarbi informacija, jeigu tokį punktą įtraukė į gyventojų surašymo
> klausimyną), sprendžiant į nurodytą buvusį pirmosios santuokos amžių
> jie susituokė 1929 metais, jie iš Lietuvos ir abiejų tėvai iš
> Lietuvos, namie iki atvykimo buvo kalbama lietuviškai, abu mokėjo
> kalbėti angliškai, dieną prieš surašymą dirbo, Jonas netarnavo
> amerikiečių kariuomenėje ir Čikagoje dirbo degalinėje (oilmen), o jo
> žmona emigravo 1913 m., gimė taip pat 1881 m. ir dirbo lošimų namuose,
> bet nesu tikra ar gerai perskaičiau raštą:

![](./media/image1.png){width="6.299305555555556in"
height="0.8506944444444444in"}

![](./media/image78.png){width="6.299305555555556in"
height="0.9708333333333333in"}

Dokumento užsakymo kortelėse jų amžius nurodytas teisingas: Jonas
Peškaitis 1935 metais buvo 44 metai, taigi gimimo metai 1891, o ne 1881,
kaip nurodyta 1930 metų gyventojų surašyme. Spėju, kad dėl kažkokių
priežasčių jie abu pasisendino 10 metų, taigi ir emigravimą greičiausiai
paankstino irgi 10 metų, vadinasi Jonas emigravo 1922 m., o Petronėlė
1923 m.

![](./media/image72.jpg){width="6.060559930008749in"
height="3.8927088801399825in"}

Petronėlės dokumentų užsakymo kortelėje 1939 metais nurodyta, kad jai 48
metai, taigi ji irgi gimė - 1891 m.

![](./media/image120.jpg){width="6.097658573928259in"
height="4.476042213473316in"}

Visuose dokumentuose, kuriuos radau nėra minima, jog jiedu turėtų vaikų.

> Peškaičių kapo paminkle gerai matomos gimimo ir mirties datos. C raidė
> po Jono vardo gali reikšti Ciprian, tai yra Kiprijonas, tai vardas
> kuriuo jis buvo pakrikštytas, bet vadinamas buvo Jonu ir dar nurodyta
> ne gimimo, o krikšto data rugsėjo 15 d.
> 
> ![](./media/image63.jpg){width="5.9125in"
> height="8.165366360454943in"}
> 
> Petronėlė Peškaitis 1891 08 17 - 1985 06 17
> 
> John C. Peškaitis 1891 09 15 - 1973 08 22

2. **Leokadija Peškaitytė** gimė 1893 m. sausio 21 diena Dimitraukos
   
   > užusienyje, pakrikštyta Daugailių bažnyčioje krikštatėviai Adomas
   > Zakarka su Leokadija Vensovaite. Kiek žinoma kartus su broliu Jonu
   > emigravo į JAV ir Čikagoje susipažino ir ištekėjo už uteniškio
   > Šaltenio. Jiedu užaugino du vaikus.[^16]

Leokadijos Peškaitytės krikšto įrašas Daugailių bažnyčios metrikos
knygoje:

![](./media/image21.png){width="6.5635422134733155in"
height="2.9500207786526684in"}

3. **Aleksandras Peškaitis** gimė 1896 m. gegužės 23 diena Dimitraukos
   užusienyje, pakrikštytas Daugailių bažnyčioje, krikštatėviai
   Norbertas Zakarka su Agota Peškaityte. Koks tolesnis likimas
   nežinoma.

Aleksandro Peškaičio krikšto įrašas Daugailių bažnyčios metrikos
knygoje:

![](./media/image91.png){width="6.6930555555555555in"
height="2.4277777777777776in"}

4. **Alfonsas Peškaitis** gimė 1899 m. liepos 2 dieną Trakelių
   
   > užusienyje, pakrikštytas Daugailių bažnyčioje, krikštatėviai
   > Žygimantas Vapas(?) su Zofija Blaževičiūte. Po II pasaulinio karo
   > su šeima gyveno Vilniuje. Užaugino sūnų Vytautą ir dukras Aldoną
   > ir Danutę. Amžino atilsio atgulė Rokantiškių kapinėse Vilniuje.

Alfonso Peškaičio krikšto įrašas Daugailių bažnyčios metrikos knygoje:

![](./media/image15.png){width="6.6930555555555555in"
height="2.6458333333333335in"}

\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_

Dar kelios Peškaičių šeimos yra paminėtos anksčiau jau minėtoje Vytauto
Indrašiaus knygoje "Antalieptės kraštas: vietovių istorinės apybraižos",
šių Peškaičių neįtraukiau į šią knygelę, nes apie juos galima
pasiskaityti šioje knygoje.

Dar Peškaičius radau rusiškame interneto puslapyje, kuriame surinkta
informacija apie SSSR politinio teroro aukas: Жертвы политического
террора в СССР.

Nežinau ar šie Peškaičiai buvo kilę iš Zarasų valsčiaus, sprendžiant iš
kaip nurodytų gimimo metų ir tėvavardžio, tai jie galėtų būti Benedikto
Peškaičio sūnaus Antano Peškaičio sūnūs. Aišku kol neradau
patvirtinančių dokumentų tai tik spėlionė.

- Пешкайтис Болеслав Антонович\
  Родился в 1898 г., Могилевская обл., г. Орша; литовцы; б/п;
  Ярцевская прядильная фабрика Смоленской области, рабочий.\
  Арестован 31 июля 1938 г. Ярцевским РО УНКВД Смоленской области\
  Приговорен: тройка УНКВД Смоленской области 14 октября 1938 г.,
  обв.: 58 - 10 ч.1.\
  Приговор: 10 лет ИТЛ Реабилитирован 19 августа 1955 г. Смоленский
  областной суд\
  Источник: Книга памяти Смоленской обл.

Boleslovas Peškaitis s. Antano

Gimė 1898 m. Oršos mieste, Mogiliovo apskrityje; lietuvis, nepartinis;
Smolensko apskritis, Jarcevo miesto verpimo fabrikas, darbininkas.

Suimtas 1938 m. liepos 31 d. Smolensko apsk. Jarcevo NKVD.

Nuteistas: Smolensko apsk. NKVD trejeto 1938 m. spalio 14 d.,
kaltinimas: 58 - 10 1 d.. (Antisovietinė propaganda ir agitacija).

Nuosprendis: 10 metų Pataisos darbų stovyklos. 1955 m. rugpjūčio 19 d.
reabilituotas Smolensko apskrities teismo.

Šaltinis: Smolensko atminties knyga.

- Пешкайтис Иосиф Антонович\
  Родился в 1896 г., Могилевская обл., г. Орша; литовцы; б/п;
  Ярцевская прядильная фабрика Смоленской области, боец пожарной
  охраны.\
  Арестован 29 июля 1938 г. Ярцевским РО УНКВД Смоленской области\
  Приговорен: тройка УНКВД Смоленской области 14 октября 1938 г.,
  обв.: 58 - 10 ч.1.\
  Приговор: 10 лет ИТЛ Реабилитирован 26 декабря 1958 г. Смоленский
  областной суд\
  Источник: Книга памяти Смоленской обл.[^17]

Juozas Peškaitis s. Antano

Gimė 1896 m. Oršos mieste, Mogiliovo apskrityje; lietuvis, nepartinis;
Smolensko apskritis, Jarcevo miesto verpimo fabrikas, ugniagesys.

Suimtas 1938 m. liepos 29 d. Smolensko apsk. Jarcevo NKVD.

Nuteistas: Smolensko apsk. NKVD trejeto 1938 m. spalio 14 d.,
kaltinimas: 58 - 10 1 d.. (Antisovietinė propaganda ir agitacija).

Nuosprendis: 10 metų Pataisos darbų stovyklos. 1958 m. gruodžio 26 d.
reabilituotas Smolensko apskrities teismo.

Šaltinis: Smolensko atminties knyga.

**Nikodemas Peškaitis 1837/1839 -- 1883 03 24**

**Grasilda Zgutaitė 1840/1842 -- 1913 01 09**

1862 sausio 7 dieną Daugailių bažnyčioje Nikodemas Peškaitis, 23 metų
amžiaus jaunuolis iš Pašekšės kaimo susituokė su Grasilda Zgutaite,
mergina 20 metų iš Mikolojūnų kaimo, Vincento Zguto ir Rozalijos
Tarulytės dukra. Bažnytinėse knygose labai aiškiai įrašyta pavardė
Згутов, bet Lietuviškų pavardžių žodyne tokios nėra. Panašiausiai skamba
Skutas, Škutas ir Gutas, tai gi kokia iš tiesų pavardė iki galo neaišku.
Dar keista, kad Grasildos mergautinę pavardę jos pačios ir jos vaikų
krikšto, mirties ir santuokos liudijimuose labai dažnai įrašydavo vis
kitokią: Kazanaitė, Kazėnaitė, Tarulytė, Gutaitė, Skututaitė\... Kodėl
jos mergautinės pavardės niekaip neatsimindavo? Mindaugas Baltuška mano,
kad Grasildos Zgutaitės šeima turėjo kažkokių sąsajų su Kazėnais, gal
buvo jų kampininkai, gal giminiavosi ar artimai bendravo ir todėl buvo
siejami su šia šeima. Dar viena prielaida: gali būti, kad pavardė buvo
reta ir palaipsniui nunyko. Tokią išvadą darau, nes Daugailių bažnyčios
1803 metų mirties metrikų knygoje radau įrašą apie Rozalijos Zgutaitės 2
metukų mergaitės iš Daugailių mirtį, taigi šioje parapijoje gyveno
daugiau žmonių su šią pavarde, o vėlesniuose metrikų knygų įrašuose
žmonių su tokia pavarde nebe pasitaiko.

Rozalijos Zgutaitės mirties įrašas 1803 m. Daugailių bažnyčios
knygoje:![](./media/image126.png){width="6.6930555555555555in"
height="0.8319444444444445in"}

Nikodemo ir Grasildos Peškaičių santuoka truko 21 metus. Santuokos
pradžioje jie gyveno Pašekšės kaime, kur prieš vesdamas gyveno
Nikodemas, apie tai sužinome iš jų pirmagimio Antano gimimo liudijimo.
Antras jų sūnus Juozapas gimė 1866 metais Mikolojūnų kaime, trečias
vaikas duktė Viktorija 1868 metais -- Murliškių užusienyje, kur tų pačių
metų pavasarį dar prieš gimstant dukrytei vienas po kito mirė Nikodemo
ir Grasildos sūneliai. Kartu su Benedikto šeima nuo 1871 metų Nikodemas
su šeima apsigyveno Trakeliuose ir nuo tada visi Nikodemo ir Grasildos
vaikai gimė Trakelių užusienyje. Matyt broliai su šeimomis kartu kūrėsi
naujoje vietoje, o vėliau kartu pirko žemės. Labai įdomi susidarė
situacija, nes abu broliai mirė tas pačiais metais. Žemę nusipirko 1883
kovo 11 diena, po 13 dienų miršta Nikodemas, po penkių mėnesių --
Benediktas, o jo žmona Agota praėjus metams ir 3 mėnesiams. Po Agotos
mirties ūkininkauti liko Nikodemo našlė Grasilda ir vyriausias Benedikto
sūnus 28 metų Mykolas, kuriam ir atiteko ūkis. Kiti Benedikto vaikai:
Marijona buvo 21 metų, Benediktas -- 19 m., Klemensui 17 m., o jaunėliai
Grasildai -- 11 m., neaišku ar tuo metu Antanas (apie 26 m.) gyveno
Trakeliuose ar jau Dusetų valsčiuje. Pasirūpinti reikėjo ir Nikodemo
našlaičiais, kurie tuo metu buvo dar visai maži: Karolis buvo 11 metų,
Agota -- 6 m., Mykolas - 3 metukų, o jaunėlė Anelė tik metukų.

Nikodemas Peškaitis mirė 1883 kovo 24 dieną Dimitraukos užusienyje
būdamas 49 metų amžiaus nuo tuberkuliozės. Po savęs paliko žmoną
Grasildą (šiame įraše jos mergautinė pavardė - Kazanaitė), vaikus Karolį
(9 m.), Mykolą (2 m.) ir Agotą (5 m.). Raštininkas neįrašė Anelės, nes
ji dar nebuvo gimusi. Palaidotas Daugailių kapinėse. Grasilda
Peškaitienė našlavo beveik 30 metų ir mirė Gutaučiuose 1913 sausio 9
dieną būdama 75 metų amžiaus, kaip įrašyta mirties liudijime nuo
senatvės. Šiame įraše jos mergautinė pavardė Tarulytė, tai jos mamos
mergautinė pavardė. Po savęs paliko vaikus: Mykolą, Agotą ir Anelę.
Palaidota Daugailių kapinėse. Kodėl Gutaučiuose? Greičiausiai gyveno
kartu su savo dukters Agotos Klevinskienės šeima.

Nikodemo Peškaičio santuokos įrašas Daugailių bažnyčios metrikos
knygoje:

![](./media/image13.png){width="6.6930555555555555in"
height="3.1694444444444443in"}

Nikodemo Peškaičio mirties įrašas Daugailių bažnyčios metrikos
knygoje:![](./media/image84.png){width="6.6930555555555555in"
height="2.0708333333333333in"}

Grasildos Peškaitienės mirties įrašas Daugailių bažnyčios metrikos
knygoje:

![](./media/image148.png){width="6.511805555555555in"
height="2.6972222222222224in"}

Nikodemas Peškaitis su žmona Grasilda Zgutaite susilaukė 9 vaikų, deja
užaugo ir savo šeimas sukūrė tik trys jauniausi vaikai: Agota,
vienintelis likęs sūnus Mykolas ir Anelė.

1. Antanas Peškaitis 1863 09 18 -- 1868 04 18

2. Juozapas Peškaitis 1866 02 07 - 1868 03 02

3. Viktorija Peškaitytė 1868 12 12 - 1882 05 16

4. Uršulė Peškaitytė 1871 05 18 - ?

5. Karolis Peškaitis 1873 10 29 - 1895 04 20

6. Jurgis Peškaitis 1876 05 15 - 1878 10 10

7. Agota Peškaitytė 1878 05 05 - 1946 01 27

8. Mykolas Peškaitis 1881 03 25 -- 1945

9. Anelė Peškaitytė 1883 11 23 -- 1962

**1. Antanas Peškaitis** gimė 1863 rugsėjo 18 dieną Pašekšės kaime,
pakrikštytas Daugailių bažnyčioje, krikštatėviai Petras Blažys su
Marijona Bonifacijaus Lamanausko žmona. Mirė Murliškėse 1868 balandžio
18 dieną, įrašyta, kad jam buvo 5 su puse metukų amžiaus, bet buvo 4 su
puse. Palaidotas Dulių kaimo kapinėse. Abiejuose įrašuose Grasildos
mergautinė pavardė: Kazanaitė (Казанов).

Antano Peškaičio krikšto įrašas Daugailių bažnyčios metrikos
knygoje:![](./media/image82.png){width="6.6930555555555555in"
height="1.5694444444444444in"}

![](./media/image18.png){width="6.6930555555555555in"
height="1.8618055555555555in"}

Antano Peškaičio mirties įrašas Daugailių bažnyčios metrikos knygoje:

![](./media/image77.png){width="6.6930555555555555in"
height="0.8993055555555556in"}

> **2. Juozapas Peškaitis** gimė 1866 vasario 7 dieną Mikolojūnų kaime,
> pakrikštytas Daugailių bažnyčioje, krikštatėviai Andrius Norkūnas su
> Elžbieta Valiukonyte. Mirė Murliškėse 1868 kovo 2 dieną, dviejų metukų
> amžiaus. Palaidotas Dulių kaimo kapinėse. Krikšto įraše Grasildos
> pavardė: Kazanaitė (Казанов), o mirties liudijimas -- teisinga
> Zgutaitė (из Згутов).

Juozapo Peškaičio krikšto įrašas Daugailių bažnyčios metrikos
knygoje:![](./media/image5.png){width="6.783333333333333in"
height="0.9951388888888889in"}

![](./media/image125.png){width="6.688194444444444in"
height="1.2548611111111112in"}

Juozapo Peškaičio mirties įrašas Daugailių bažnyčios metrikos knygoje:

![](./media/image114.png){width="6.6930555555555555in"
height="1.1520833333333333in"}

1. **Viktorija Peškaitytė** gimė 1868 metų gruodžio 23 dieną
   
   > Murliškėse, pakrikštyta Daugailių bažnyčioje krikštatėviai Jonas
   > Zakarka su Marijona Bonifacijaus Lamanausko žmona. Čia Grasildos
   > pavardė įrašyta neteisinga -- Kazanaitė (Казанов). Tik gimusi
   > Viktorija Nikodemo ir Grasildos šeimoje tapo vyriausiu vaiku, nes
   > du pirmieji jų sūnūs mirė tų pačių 1868 metų pavasarį. Gaila, bet
   > Viktorija taip pat mirė vaikystėje 1882 metų gegužės 16 dieną
   > Mitraukoje nuo raupų būdama 13 metų, nors mirties įraše nurodyta,
   > kad jai buvo 14 metų. Mirties įraše jos mamos Grasildos mergautinė
   > pavardė teisinga: Zgutaitė (из Згутов) Palaidota Daugailių
   > kapinėse.

Viktorijos Peškaitytės krikšto įrašas Daugailių bažnyčios metrikos
knygoje:![](./media/image113.png){width="6.7243055555555555in"
height="1.2715277777777778in"}![](./media/image112.png){width="6.58125in"
height="1.5451388888888888in"}

Viktorijos Peškaitytės mirties įrašas Daugailių bažnyčios metrikos
knygoje:

![](./media/image115.png){width="6.6930555555555555in"
height="2.265972222222222in"}

> **4. Uršulė Peškaitytė** gimė 1871 metų gegužės 18 dieną Trakelių
> užusienyje, pakrikštyta Daugailių bažnyčioje krikštatėviai Bonifacijus
> Lamanauskas su Liudvika Juozapo Juodvalkio žmona. Šiame įraše
> Grasildos mergautinė pavardė -- Kazanaitė (из Казанов). Kada mirė
> neaišku, bet jos vardo nėra tarp likusių vaikų po Nikodemo mirties.

Uršulės Peškaitytės krikšto įrašas Daugailių bažnyčios metrikos knygoje:

![](./media/image132.png){width="6.725694444444445in"
height="1.3131944444444446in"}

**5. Karolis Peškaitis** gimė 1873 metų spalio 29 dieną Trakelių
užusienyje, pakrikštytas Daugailių bažnyčioje, krikštatėviai Jonas
Ilčiukas su Barbora Zakarkaite. Čia vienas iš tų įrašų, kur Grasildos
mergautinė pavardė įrašyta teisingai --
Zgutaitė.![](./media/image142.png){width="6.711805555555555in"
height="1.20625in"}

1890 lapkritį buvo savo pusbrolio Mykolo Peškaičio santuokos liudininku.
Mirė Trakeliuose (Mitrauka) 1895 balandžio 20 dieną nuo tuberkuliozės,
kaip įrašyta 22 metų amžiaus. Mirties įraše jo mamos Grasildos
mergautinė pavardė: Gudisaitė (Гудисов). Išties jam buvo 21 su puse.
Palaidotas Daugailių kapinėse.

Karolio Peškaičio krikšto įrašas Daugailių bažnyčios metrikos knygoje:

![](./media/image49.png){width="6.747916666666667in"
height="1.0256944444444445in"}![](./media/image151.png){width="6.809722222222222in"
height="1.273611111111111in"}

Karolio Peškaičio mirties įrašas Daugailių bažnyčios metrikos knygoje:

![](./media/image8.png){width="6.6930555555555555in"
height="1.832638888888889in"}

> **6. Jurgis Peškaitis** gimė 1876 metų gegužės 15 dieną Trakelių
> užusienyje, pakrikštytas Daugailių bažnyčioje, krikštatėviai Jurgis
> Lamanauskas su Marijona Peškaityte. Jurgio krikšto įraše jo mamos
> Grasildos mergautinė pavardė: Skututaitė (Скутутов), o mirties įraše
> teisinga: Zgutaitė (Згутовна). Mirė 1878 spalio 10 dieną nuo
> tuberkuliozės būdamas 2 su puse metukų amžiaus, Mitraukos užusienyje.
> Palaidotas Daugailių kapinėse.

Jurgio Peškaičio krikšto įrašas Daugailių bažnyčios metrikos knygoje:

![](./media/image140.png){width="6.679861111111111in"
height="1.8555555555555556in"}![](./media/image4.png){width="6.677083333333333in"
height="2.3513888888888888in"}

Jurgio Peškaičio mirties įrašas Daugailių bažnyčios metrikos knygoje:

![](./media/image19.png){width="6.6930555555555555in"
height="1.8645833333333333in"}

> **7. Agota Peškaitytė Klevinskienė** gimė 1878 metų gegužės 5 dieną
> Antalieptės valsčiaus, Ignalinos bendruomenės, Trakelių užusienyje,
> pakrikštyta Daugailių bažnyčioje, krikštatėviai Jurgis Lamanauskas su
> Eleonora Juodvalkyte. Šiame įraše Grasildos mergautinė pavardė:
> Kazėnaitė (Казеновной). Nors Agota buvo septintas Nikodemo ir
> Grasildos vaikas, nuo 1895 metų mirus jos vyresniam broliui Karoliui,
> ji liko vyriausia. Ištekėjo už Mykolo Klevinsko (gimusio 1872 02 13)
> iš Padusčio kaimo Antalieptės valsčiuje. Iš vaikų krikšto įrašų
> sužinome, kad po santuokos su vyru gyveno Gutaučiuose.
> 
> Agota Klevinskienė mirė 1946 sausio 27 dieną nuo žarnų užsisukimo
> eidama 68 metus, jos vyras Mykolas irgi mirė apie 1946 metus. Šią
> informaciją ir nuotraukas radau internete geni.com svetainėje, kur
> žmonės nemokamai sudarinėja savo giminės medžius.

Agotos Peškaitytės krikšto įrašas Daugailių bažnyčios metrikos knygoje:

![](./media/image81.png){width="6.6930555555555555in"
height="2.923611111111111in"}

Agota ir Mykolas Klevinskai su dukterimis.

Agotos pasas išduotas vokiečių I pasaulinio karo
metu.![](./media/image65.jpg){width="2.5639916885389327in"
height="4.104166666666667in"}

![](./media/image145.jpg){width="3.688542213473316in"
height="3.027678258967629in"}

Agota su savo vyru Mykolu susilaukė septynių vaikų, gaila tik visi sūnūs
mirė, užaugo ir savo šeimas sukūrė keturios dukterys:

> 1\. Ona Kleviniskaitė Pratkelienė (1905 - 2001) 1938 vasario 27 dieną
> Daugailių bažnyčioje ištekėjo už Broniaus Pratkelio (1902 -- 1952). Su
> vyru užaugino Algirdą Pratkelį (g. 1929), Evaldą Pratkelį (1932 --
> 1983), Mindaugą Vladą Pradkelį (1938 11 09 -- 2017 04 26) ir Aldoną
> Pratkelytę Guntulienę (1942 05 22 -- 2018 02).
> 
> ![](./media/image80.png){width="4.014583333333333in"
> height="4.495138888888889in"}

Ona Klevinskaitė ir Bronius Pratkelis

> 2\. Zofija Klevinskaitė Valskienė gimė Gutaučiuose 1906 birželio 30
> dieną, pakrikštyta Daugailių bažnyčioje, krikštatėviai Vincentas
> Namajunas su Viktorija Palvelytė. 1938 vasario 27 dieną Daugailių
> bažnyčioje ištekėjo už Prano Valskio (1904 - ?), mirė 1998 metais. Po
> savęs paliko dukra Danutę Valskytę Tušienę.

Zofijos Klevinskaitės krikšto įrašas Daugailių bažnyčios metrikos
knygoje:![](./media/image121.png){width="6.6930555555555555in"
height="3.0930555555555554in"}

Prieraše kunigas suklydo užrašęs Pranciškaus pavardę Klevinskas, o ne
Valskys:

> ![](./media/image27.png){width="3.2083333333333335in"
> height="1.8541666666666667in"}

Zofijos Klevinskaitės ir Prano Valskio vestuvės 1939 m.

![](./media/image20.jpg){width="5.021875546806649in"
height="3.28245406824147in"}

3\. Petras Klevinskas gimė Gutaučiuose 1908 birželio 29 dieną,
pakrikštytas Daugailių bažnyčioje, krikštatėviai Vincentas Žurauskas su
Elena Peškaityte. 1937 metais mirė nuo plaučių uždegimo. Nesu tikra ar
tai jo nuotrauka geni.com puslapyje prie šios nuotraukos įrašytas Petro
Klevinsko vardas, o A. Indrašiaus knygoje prie tos pačios nuotraukos
įrašytas kito žmogaus vardas.

Petro Klevinsko krikšto įrašas Daugailių bažnyčios metrikos knygoje:

![](./media/image150.png){width="6.6930555555555555in"
height="2.341666666666667in"}

![](./media/image96.png){width="2.1680555555555556in"
height="2.8756944444444446in"}

> 4\. Elžbieta Klevinskaitė Kanapeckienė gimė Gutaučiuose 1911 gegužės
> 22 dieną pakrikštyta Daugailių bažnyčioje, krikštatėviai Antanas
> Blažys su Anele Peškaityte. 1942 rugsėjo 8 dieną Daugailių bažnyčioje
> ištekėjo už Jono Kanapecko gimusio Jakštų kaime 1902 liepos 20 dieną.
> Gilioro Kanapecko ir Konstancijos Raišytės sūnaus. Elžbieta mirė 2003
> metais, po savęs paliko dukrą Liną Kanapeckaitę Ramelienę.

Elžbietos Klevinskaitės krikšto įrašas Daugailių bažnyčios metrikos
knygoje:

![](./media/image40.png){width="6.6930555555555555in"
height="2.96875in"}

Prierašas prie krikšto įrašo apie santuoką Daugailių bažnyčios metrikos
knygoje:

![](./media/image3.png){width="3.522222222222222in"
height="1.2902777777777779in"}![](./media/image59.png){width="1.9572911198600176in"
height="4.742668416447944in"}

![](./media/image83.png){width="1.6989588801399824in"
height="2.0872922134733156in"}

Elžbieta Klevinskaitė

> 5\. Povilas Klevinskas gimė Gutaučiuose 1914 balandžio 2 dieną,
> pakrikštytas Daugailių bažnyčioje, krikštatėviai Petras Šileikis su
> Kotryna Antano Blažio sutuoktine. Mirė Gutaučiuose 1915 vasario 16
> dieną būdamas vos 10 mėnesių amžiaus, kaip įrašyta metrikų knygoje nuo
> silpnumo. Palaidotas Daugailių kapinėse.

Povilo Klevinsko krikšto įrašas Daugailių bažnyčios metrikos
knygoje:![](./media/image68.png){width="6.6930555555555555in"
height="3.0770833333333334in"}

Povilo Klevinsko mirties įrašas Daugailių bažnyčios metrikos knygoje:

![](./media/image51.png){width="6.6930555555555555in"
height="2.160416666666667in"}

> 6\. Antanas Klevinskas gimė Gutaučiuose 1916 spalio 25 dieną,
> pakrikštytas Daugailių bažnyčioje, krikštatėviai Jonas Mikulionas su
> Marijona Ignoto Namajuno žmona. Mirė Gutaučiuose 1917 birželio 17
> dieną taip pat nuo silpnumo vos 6 mėnesių amžiaus. Palaidotas
> Daugailių kapinėse.

Antano Klevinsko krikšto įrašas Daugailių bažnyčios metrikos knygoje:

![](./media/image129.png){width="6.6930555555555555in"
height="2.502083333333333in"}

Antano Klevinsko mirties įrašas Daugailių bažnyčios metrikos knygoje:

![](./media/image64.png){width="6.6930555555555555in"
height="2.7159722222222222in"}

> 7\. Teklė Kleviniskaitė Ulevičienė gimė 1918 gruodžio 14 dieną
> Gutaučiuose, pakrikštyta Daugailių bažnyčioje, krikštatėviai Antanas
> Blažys su Anele Šileikyte. 1949 sausio 1 dieną Vilniuje ištekėjo už
> Jono Ulevičiaus gimusio 1909 lapkričio 27 dieną. 1980 rugpjūčio 11
> dieną Jonas mirė nuo ūmaus miokardo infarkto. Teklė Ulevičienė mirė
> 2006 rugsėjo 6 dieną nuo širdies ir plaučių veiklos nepakankamumo. Po
> savęs paliko Ireną Ulevičiūtę Konopackienę (g. 1949 04 12) ir Henriką
> Ulevičių (g. 1952 10 03).

Teklės Klevinskaitės krikšto įrašas Daugailių bažnyčios metrikos
knygoje:

![](./media/image41.png){width="6.6930555555555555in"
height="3.415277777777778in"}

Teklės ir Jono Ulevičių šeima: Teklė Ulevičienė tarp giminaičių:\
![](./media/image24.jpg){width="2.6064709098862644in"
height="3.621874453193351in"}![](./media/image10.jpg){width="2.420252624671916in"
height="3.625in"}

Irena Ulevičiūtė 1978 liepos 28 dieną ištekėjo už Leopoldo Konopacko,
(g. 1947 10 21), Stanislovo Konopacko ir Elenos Buchvicaitės sūnaus.
1979 spalio 10 dieną jiems gimė dukrelė Rūta Konopackaitė, kuri mirė
visai mažutė 1983 02 14. O 1987 metų lapkričio 25 dieną susilaukė sūnaus
Povilo Konopacko.![](./media/image138.png){width="3.1979166666666665in"
height="4.411111111111111in"}

> Irena ir Leopoldas Konopackai

Henrikas Ulevičius vedė Jūratę Šležaitę, Mykolo Šležo ir Valerijos
Šulskytės dukrą. Jiedu susilaukė Dovilės Ulevičiūtės ir Donato
Ulevičiaus.

> ![](./media/image87.jpg){width="2.426388888888889in"
> height="3.4625in"}
> 
> Jūratė ir Henrikas Ulevičiai su sūneliu Donatu.
> 
> **9. Anelė Peškaitytė** gimė 1883 lapkričio 23 dieną Trakelių
> užusienyje, krikštatėviai Ignotas Saulevičius su Marijona Peškaityte.
> Ji gimė praėjus 8 mėnesiams po savo tėvo mirties, o kadangi buvo
> jauniausias vaikas abiejose šeimose, tai gauti kraitį matyt buvo
> sudėtinga, taip galvoju, nes ji ištekėjo už jau nebejauno 52 metų
> amžiaus našlio Rapolo Valiukėno 1912 vasario 5 dieną Daugailių
> bažnyčioje, Antano Valiukėno ir Teresės Tarulytės sūnaus. Anelė
> ištekėjo būdama 28 metų amžiaus, bet santuokos liudijime įrašyta - 30
> metų. Nežinau ar jai įžengus į vyro namus visi Rapolo vaikai gyveno
> kartu, juk vyriausiems Rapolo vaikams buvo 25, 23 ir 18 metų. Tik
> dviem jauniausioms mergaitėms 15 ir 9 metų ji turėjo pakeisti mamą.

Anelės Peškaitytės krikšto įrašas Daugailių bažnyčios metrikos knygoje:

![](./media/image50.png){width="5.7822922134733155in"
height="3.1201476377952755in"}

Anelės Peškaitytės santuokos įrašas Daugailių bažnyčios metrikos
knygoje:

![](./media/image146.png){width="5.4072922134733155in"
height="4.684076990376203in"}

> Kadangi Rapolo Valiukėno vaikai iš pirmosios santuokos tapo Anelės
> posūniais ir podukromis, o gimusiems Anelės sūnums - įbroliais ir
> įseserėmis, tai surašiau Rapolo Valiukėno ir jo pirmosios žmonos
> Apolonijos Milašauskaitės vaikus.
> 
> Rapolas Valiukėnas gimė apie 1860 metus. 1886 lapkričio 27 dieną
> Daugailių bažnyčioje susituokė su Apolonija Milašauskaite, 20 metų
> mergina Konstantino Milašausko ir Teresės Kugelytės dukra iš Drąsėnų
> kaimo. Apolonija Milašauskaitė gimė 1865 12 01 Drąsėnuose, pakrikštyta
> Daugailių bažnyčioje, krikštatėviai Jonas Ribokas su Apolonija Adomo
> Milašausko sutuoktine. Rapolo pirmoji santuoka truko 24 metus per
> kuriuos su žmona Apolonija susilaukė 8 vaikų:
> 
> 1\. Nikodemas Valiukėnas 1887 09 11 gimė Mikolojūnų kaime,
> pakrikštytas Daugailių bažnyčioje, krikštatėviai Stanislovas Čepėnas
> su Adele Valiukėnaite.
> 
> 2\. Alfonsas Valiukėnas 1889 12 01 gimė Mikolojūnų kaime, pakrikštytas
> Daugailių bažnyčioje, krikštatėviai Baltramiejus Markevičius su
> Marijona Dominyko Dambrausko žmona.
> 
> 3\. Emilija Valiukėnaitė 1892 03 11 gimė Mikolojūnų kaime, pakrikštyta
> Daugailių bažnyčioje, krikštatėviai Nikodemas Burterlavičius su
> Apolonija Milašauskaite. 1892 rugsėjo 6 dieną mirė Mikolojūnuose nuo
> silpnumo pusės metukų amžiaus. Palaidota Daugailių kapinėse.
> 
> 4\. Zofija Valiukėnaitė 1893 06 19 gimė Mikolojūnų kaime, pakrikštytas
> Daugailių bažnyčioje, krikštatėviai Dominykas Dambrauskas su Zofija
> Buterlavičiūte. 1893 rugsėjo 5 dieną mirė nuo silpnumo 2 mėnesių
> amžiaus. Palaidota Daugailių kapinėse.
> 
> 5\. Ona Valiukėnaitė 1894 09 25 gimė Mikolojūnų kaime, pakrikštytas
> Daugailių bažnyčioje, krikštatėviai Jurgis Valiukėnas su Ona
> Stanislovo Čepėno žmona.
> 
> 6\. Uršulė Valiukėnaitė 1897 02 20 gimė Mikolojūnų kaime, pakrikštytas
> Daugailių bažnyčioje, krikštatėviai Andrius Morkūnas su Justina Mykolo
> Simonavičiaus žmona.
> 
> 7\. Jonas Valiukėnas 1900 01 24 gimė Mikolojūnų kaime, pakrikštytas
> Daugailių bažnyčioje, krikštatėviai Juozas Tarulis su Marijona
> Valiukėnaitė. 1901 birželio 7 dieną mirė Mikolojūnuose nuo kokliušo
> pusantrų metukų amžiaus. Palaidotas Daugailių kapinėse.
> 
> 8\. Karolina Valiukėnaitė 1903 05 23 gimė Mikolojūnų kaime,
> pakrikštytas Daugailių bažnyčioje, krikštatėviai (neperskaitau)
> Grasilda Jurgio Valiukėno žmona.
> 
> Apolonija Milašauskaitė Valiukonienė mirė 1910 lapkričio 18 dieną nuo
> reumatizmo apie 42 metų amžiaus, iš tiesų buvo 45 metų. Po savęs
> paliko vyrą Rapolą Valiukėną ir penkis vaikus: Nikodemą, Alfonsą, Oną,
> Uršulę ir Karoliną. Palaidota Daugailių kaimo kapinėse. Kol kas nieko
> neradau internete apie jų tolimesnį gyvenimą.

Apolonijos Milašauskaitės krikšto įrašas Daugailių bažnyčios metrikos
knygoje:

![](./media/image7.png){width="6.6930555555555555in"
height="0.9972222222222222in"}![](./media/image67.png){width="6.6930555555555555in"
height="0.9131944444444444in"}

Rapolo Valiukėno 1 santuokos įrašas Daugailių bažnyčios metrikos
knygoje:

![](./media/image102.png){width="6.6930555555555555in"
height="4.352083333333334in"}

Apolonijos Milašauskaitės Valiukėnienės mirties įrašas Daugailių
bažnyčios metrikos knygoje:

![](./media/image9.png){width="6.6930555555555555in"
height="2.520138888888889in"}

> Anelė Peškaitytė su Rapolu Valiukėnu susilaukė trijų sūnų:
> 
> 1\. Antanas Valiukėnas gimė Mikolojūnuose 1913 vasario 4 dieną,
> pakrikštytas Daugailių bažnyčioje, krikštatėviai Rapolas Samanavičius
> su Agota Mykolo Klevinsko sutuoktine (Anelės sesuo). 1937 vasario 4
> dieną Daugailių bažnyčioje susituokė su Bronislava Krasauskaite (1911
> 07 17 -- 1999 01 25). Antanas Valiukėnas 1945 vasario 22 dieną žuvo
> Latvijoje netoli Priekulės. Po savęs paliko sūnūs Mindaugą (1938 --
> 1995) ir Bronių (g. 1942).

Antano Valiukėno krikšto įrašas Daugailių bažnyčios metrikos knygoje:

![](./media/image39.png){width="6.6930555555555555in"
height="3.3291666666666666in"}

Prierašas apie santuoką prie krikšto įrašo Daugailių bažnyčios metrikos
knygoje:

![](./media/image30.png){width="4.156944444444444in"
height="1.7083333333333333in"}

> 2\. Juozas Valiukėnas gimė Mikolojūnuose 1916 kovo 20 dieną
> pakrikštytas Daugailių bažnyčioje, krikštatėviai Mykolas Klevinskas su
> Ona Valiukėnaite. 1944 gegužės 15 dieną Kauno civilinės metrikacijos
> biure vedė Zitą Salomėją Vidginytę (? -- 1953). Po žmonos mirties vedė
> antrą kartą Stefaniją Vedegienę. Mirė Kaune 1976 metais, po savęs
> paliko vaikus iš pirmosios santuokos: Raimundą Valkiukėną, Daivą
> Valiukėnaitę, Antaną Valiukėną ir iš antrosios - Lionginą Valiukėną.

Juozo Valiukėno krikšto įrašas Daugailių bažnyčios metrikos knygoje:

![](./media/image34.png){width="6.6930555555555555in"
height="2.7715277777777776in"}

Prierašas prie krikšto įrašo apie santuoką Daugailių bažnyčios metrikos
knygoje![](./media/image25.png){width="3.7020833333333334in"
height="1.8375in"}

> 3\. Mečislovas Valiukėnas gimė Mikolojūnuose 1920 rugsėjo 9 dieną,
> pakrikštytas Daugailių bažnyčioje, krikštatėviai Antanas Blažys su
> Sofija Valiukėnaite. Kaip susiklostė tolimesnis likimas nežinoma.

Mečislovo Valiukėno krikšto įrašas Daugailių bažnyčios metrikos knygoje:

![](./media/image36.png){width="6.6930555555555555in"
height="3.609027777777778in"}

Anelė Valiukėnienė liko našle tik kada tiksliai negaliu pasakyti. Iš
prierašo prie jos krikšto įrašo sužinome, kad 1924 vasario 24 dieną
Daugailių bažnyčioje susituokė su Norbertu Jurkevičiumi, taigi ištekėjo
antrą kartą, kaip susiklostė tolesnis jos gyvenimas nežinoma. Anelė
Jurkevičienė mirė 1962 metais. Šį informacija iš internetinės svetainės
geni.com.

> Prierašas apie santuoką prie Anelės Peškaitytės gimimo liudijimo
> Daugailių bažnyčios krikšto knygoje:

![](./media/image42.png){width="3.6468755468066494in"
height="0.8320538057742782in"}

**8. Mykolas Peškaitis** gimė 1881 kovo 25 dieną Trakelių užusienyje,
krikštatėviai Antanas Zakarka su Marijona Adomo Zakarkos žmona. Mykolas
buvo aštuntas Nikodemo ir Grasildos Peškaičiu vaikas, bet jam gimus
Peškaičių šeimoje tebuvo du vaikai: Karolis ir Agota, tai gi Mykolas -
trečias, o nuo 14 metų 1895 m., mirus broliui Karoliui -- antras ir
vienintelis sūnus pratęsęs giminę. Tik va augo Mykolas auklėjamas tik
mamos ir pusbrolio Mykolo, kartu su broliu Karoliu, seserimis Agota ir
Anele, pusbroliais Benediktu, Klemensu ir pussesere Grasilda. Mykolo
tėvas Nikodemas mirė, kai Mikui tebuvo 2 metukai.

> Mykolo Peškaičio krikšto įrašas Daugailių bažnyčios metrikų knygoje:

![](./media/image101.jpg){width="6.653472222222222in"
height="2.76875in"}

1903 lapkričio 10 diena Daugailių bažnyčioje susituokė Antalieptės
valsčiaus valstietis Mykolas Peškaitis 23 metų amžiaus iš Dimitraukos
užusienio su Novalaeksandrovsko (Zarasų) miestiete Emilija
Kryžinauskaite 23 metų amžiaus iš Madagaskaro užusienio. Liudininkai:
Mykolas Zakarka, Juozapas Zakarka, Justinas Tarulis ir Antanas Tarulis.

Mykolo Peškaičio ir Emilijos Kryžinauskaitės santuokos įrašas Daugailių
bažnyčios metrikų
knygoje:![](./media/image136.jpg){width="6.0947922134733155in"
height="4.560936132983377in"}

**Kryžinauskai.** Justinas Kryžinauskas gimęs apie 1855 metus buvo geras
stalius. Kaip ir kiti to meto amatininkai eidavo per žmones ir jiems
gamino tai, ko jiems reikėjo. Kai 1879 lapkričio 10 dieną gimė Emilija
jie gyveno Gojaus užusienyje, kuris priklausė Zarasų, tada
Novoaleksandrovsko miesto dūmai. Emilijos krikšto ir santuokos metrikose
įrašyta, kad Justinas Kryžinauskas su žmona Anastasija Pakalnyte buvo
Novoaleksandrovsko tai yra Zarasų miestiečiai, bet gyveno ne mieste,
todėl labai sunku atsekti kur ir kada jie gimė, susituokė, krikštijo
savo vaikus. Kol kas radau tik Emilijos Kryžinauskaitės, Algirdo
Peškaičio močiutės, gimimo įrašą ir tai tik todėl, kad ji buvo
pakrikštyta Daugailių bažnyčioje.

Emilijos Kryžinauskaitės krikšto įrašas Daugailių bažnyčios metrikų
knygoje:

![](./media/image32.png){width="6.653472222222222in"
height="2.109722222222222in"}

1879 lapkričio 10 dieną Novoaleksandrovsko (Zarasų) miesto dūmos Gojaus
užusienyje gimė Emilija, Justino Kryžinausko ir Anastasijos Pakalnytės
duktė. Pakrikštyta Daugailių bažnyčioje, krikštatėviai Juozapas
Malinauskas su Agota Felikso Kryžinausko sutuoktine.

Vėliau Justino Kryžinausko šeima gyveno Utenos apskrities bažnytkaimyje
Sudeikiuose prie Alaušo ežero, taip pasakoja Justino proanūkis Algirdas
Peškaitis. Apie 1935 metus Justinas Kryžinauskas su žmona Anastasija ir
anūke Petronėle (Petre) atsikėlė gyventi į Zokorius pas savo dukrą
Emiliją. Į Zokorius jie perkėlė ir savo namuką, kuris ir dabar ten
tebestovi - vadinamas gryčiute.

Justinas Kryžinauskas, per savo anūkės Petrės vestuves susirgo ir
greitai mirė, tai buvo 1936 metų rugpjūčio 15 dieną. Jo žmona
Anastasija, kuri į senatvę apako, taip ir gyveno iki pat savo mirties
1942 metais toje gryčiutėje, kadangi ji mirė karo metais, tai paminklo
nebuvo už ką pakeisti, matyt galvojo, kad tai padarys vėliau. Įdomu, kad
visi giminėje žino, jog čia palaidoti abu proseneliai. Kaip sakoma nieko
nėra pastoviau už laikinumą, tai ir paminklas liko tik su Justino
Kryžinausko pavarde Daugailių kapinėse.

![](./media/image154.png){width="3.6795450568678914in"
height="4.892707786526684in"}

> Justino ir Anastasijos gim. Pakalnyte Kryžinauskų kapas Daugailių
> kapinėse. Nuotrauka daryta 2019 m.

Mykolo anūkas Algirdas Peškaitis pasakoja, kad maždaug 1902 metais
Mykolas buvo paimtas į carinės Rusijos kariuomene.

Rusijos imperijoje po 1874 metų karinės reformos į kariuomenę buvo imami
vyrai sulaukę 20 metų, taigi Mykolas turėjo eiti į kariuomenę 1901 m.
Nuo išsilavinimo priklausė kiek metų reikia tarnauti: iš viso reikėjo
atitarnauti sausumos kariuomenėje 6 metus, jeigu baigei pradžios mokyklą
-- 4 metus, miesto mokyklą -- 3 m, o jei gimnaziją -- 1,5 m., o su
aukštuoju išsilavinimu tarnyba trukdavo puse metų, o nuo 1906 metų
balandžio įsigaliojo naujas įstatymas, pagal kurį karinė prievolė
sausumos kariuomenėje sutrumpėjo iki 3 metų tarnybos.[^18] Nėra aišku
kada tiksliai Mykolas išėjo į tarnybą, bet galime iš netiesioginių faktų
nustatyti kada tai įvyko. Pirma: Mykolo Peškaičio santuokos, įvykusios
1903 metų lapkričio 10 dieną, liudijime socialinė Mykolo padėtis --
valstietis, o ne kareivis (солдат), antra: pirmagimio Antano jie
susilaukė tik 1907 metais. Tai patvirtina mano spėjimą, kad jis
greičiausiai į kariuomenę buvo paimtas jau po santuokos. Taigi manyčiau,
kad Mykolas tarnybą atliko 1904 - 1906 m. Neaišku viena, kur tuos metus
gyveno Mykolo žmona Emilija?

Iš Algirdo Peškaičio pasakojimo žinome, kad pulkas, kuriame tarnavo
Mykolas buvo dislokuotas Jekaterinburgo mieste, kuris sovietiniais
laikais buvo pervadintas į Sverdlovską, o šiuo metu ir vėl susigrąžino
ankstesnį pavadinimą. Kuriame iš carinės Rusijos kariuomenės pulkų
tarnavo Mykolas Peškaitis neaišku, jo tarnybos laikotarpiu vyko 1904 -
1905 metų Rusijos - Japonijos karas ir po to 1905 metų revoliucija.
Jekaterinburgas tapo kariuomenės paskirstymo punktu, ten 1904 metų
gegužę iš 2 bataliono rezervinio 214 pulko organizuojamas 282
Černojarsko pėstininkų pulkas, kuris išvyko Mandžiūriją. Tuo pačiu metu
Jekaterinburge iš mobilizuotų karių formuojamas 222 atsarginis
pėstininkų batalionas, kuris 1905 m. sausį pervadintas į 38 Rytų Sibiro
atsargos batalioną. 1905 balandį šis pulkas išvyko į tolimuosius Rytus
ir 1905 gegužės 17 dieną atvyko į Charbino miestą (dabar priklauso
Kinijai)[^19]. 1906 metų gegužę į Jekaterinburgą grįžo 214 Mokšansko
rezervinis pulkas, kuris mieste dislokavosi iki metų pabaigos. 1906
rugsėjį iš Samaros atvyko 244 Borisovsko rezervinis pulkas.[^20] Tai
viskas ką man pavyko rasti internete apie to laikotarpio karinių dalinių
padėtį Jekaterinburgo mieste. Rusijoje tik paskutiniais metais pradėta
domėtis laikotarpiu senesniu nei 1917 metų spalio revoliucija. Galima
sakyti visą mus dominančia medžiagą, kurią man pavyko surasti, internete
atsirado tik entuziastų dėka, todėl trūksta ne tik duomenų, bet tokių
atrodytų elementarių dalykų, kaip įkeltų nuotraukų padarymo datos.
Jekaterinburgo nuotraukos įkeltos į internetą, su keliom išimtim, taip
pat neaišku kada padarytos. Visada žinojau apie tai, kad Rusijoje
bolševikai masiškai naikino viską: buvo sugriauti ištisi miestų
kvartalai, bažnyčios išgrobstytos, pastatai perstatyti arba tiesiog
susprogdinti, jau nekalbu apie masinius žmonių trėmimus ir žudimus. Šitų
naikinimų pasekmės labai pasijuto man ieškant duomenų apie Mykolo
Peškaičio šeimos gyvenimą nuo 1906 metų iki 1923 metų, kai jie gyveno
Rusijoje. ![](./media/image127.jpg){width="5.7197922134733155in"
height="2.464882983377078in"}

Jekaterinburgo
kareivinės.[^21]![](./media/image133.jpg){width="5.834375546806649in"
height="3.266524496937883in"}

![](./media/image12.jpg){width="5.7197922134733155in"
height="4.367679352580927in"}

Taip atrodė imperinės Rusijos kariai 1905 m. Manyčiau, kad tokią
uniformą vilkėjo ir Mykolas Peškaitis savo tarnybos metu. Nuotrauka iš
Sankt Peterburgo nuotraukų archyvo.

Spėju, kad Mykolas Peškaitis galėjo būti paskirtas į 38 Rytų Sibiro
atsargos batalioną ir 1905 m. išvyko į Charbiną. Mano spėjimas remiasi
Algirdo Peškaičio pasakojimu: Jekaterinburgo mieste karinę tarnybą
atliko ir Algirdo močiutės Emilijos Kryžinauskaitės brolis Jonas
Kryžinauskas, kuris tarnybą atliko raštinėje, nes gražiai rašė.
Pasakojime sakoma, kad Emilija atvyko pas brolį ir ten gyvendama
prižiūrėjo karininkų vaikus. Ir būtent ten Emilija susipažino su Mykolu
ir jų romantiška pažintis baigėsi vestuvėmis Jekaterinburgo mieste. Man
buvo neaišku, kodėl netekėjusi mergina viena važiavo aplankyti brolį
kažkur už Uralo kalnų? Kai radau Peškaičių santuokos liudijimą, viskas
stojo į savo vietas - 1903 metais Daugailių bažnyčioje Emilija ištekėjo
už Mykolo Peškaičio, taigi tapo aišku kodėl ji ryžosi tokiai kelionei.
Manyčiau ji važiavo pas vyrą, bet jai atvykus į Jekaterinburgą Mykolas
matyt jau buvo išvykęs į Tolimuosius Rytus, todėl ir pasakojama, kad ji
atvažiavo pas brolį, kuris ir pasirūpino seserimi. Žinoma dabar galima
tik spėlioti, o kaip buvo iš tiesų - nežinia.

Jekaterinburgo miestas šiuo metu yra ketvirtas miestas pagal gyventojų
skaičių Rusijoje, 2012 metų duomenimis mieste gyveno 1,387 milijonų
gyventojų, todėl labai nustebino kiek mažai informacijos apie XX amžiaus
pradžios miesto istoriją yra internete: 1723 metais prie metalurgijos
gamyklos įkurta gyvenvietė ir šalia pastatyta tvirtovė. Miestas
pavadintas Jekaterinburgu imperatoriaus Petro I žmonos Jekaterinos,
būsimosios imperatorės, vardu. Miesto teisės nuo 1796 m. Nuo 18 a. Uralo
kasybos pramonės centras. 1918 05--07 bolševikai čia kalino ir 07 17
nužudė paskutinį Rusijos imperatorių Nikolajų II ir jo šeimą. [^22]

Jekaterinburgas 1909 metais padaryta spalvota nuotrauka, 2014 m.
restauruota:

![](./media/image99.jpg){width="6.4072922134733155in"
height="5.570258092738408in"}

Jekaterinburgas, gatvės gale šv. Onos bažnyčia, kurioje Peškaičiai
krikštijo savo
vaikus:![](./media/image76.jpg){width="6.0010422134733155in"
height="4.512556867891513in"}

Jekaterinburgo Romos - katalikų šv. Onos bažnyčia vėliau buvo
aptinkuota[^23]:

![](./media/image144.jpg){width="2.6468755468066494in"
height="3.757617016622922in"}

Šv. Onos bažnyčia buvo pastatyta už suaukotus pinigus 1884 metais. Tuo
metu mieste buvo nedidelė lenkų katalikų bendruomenė, kuri parašė raštą
imperatoriui Nikolajui I su prašymu skirti žemės bažnyčios statybai.

Šv. Onos bažnyčia Jekaterinburge, kuris buvo vadinamas lenkiška
bažnyčia:

![](./media/image93.jpg){width="6.053125546806649in"
height="3.7937937445319334in"}

Šv. Onos bažnyčia, gaila neaišku kuriais metais:

Šv. Onos bažnyčia paversta autobusų stotimi XX a. 7
dešimtmetyje:![](./media/image29.jpg){width="6.695833333333334in"
height="4.555555555555555in"}

![](./media/image56.jpg){width="6.695833333333334in"
height="4.472222222222222in"}

Bažnyčia buvo vienu iš aukščiausiu pastatu mieste, o altorių puošė šv.
Onos ikona. Jekaterinburgo šv. Onos bažnyčia buvo miestiečių traukos
centras, viliojanti šeimas su vaikais uksminga pasivaikščiojimo alėja ir
vargonais. Iki I Pasaulinio karo pradžios bažnyčios parapijiečių buvo
virš tūkstančio, kurių tarpe buvo ir į šiuos kraštus atklydę lietuviai.
Parapijiečių padaugėjo prasidėjus I pasauliniam karui, bet po 1917 metų
revoliucijos bažnyčia buvo išgrobstyta - iš jos buvo išnešta viskas, kas
turėjo nors menkiausia vertę. Tik keletas daiktų vėliau pateko į
muziejų.[^24] Prasidėjo ir pačių katalikų persekiojimai, o paskutinis
bažnyčios klebonas Pranciškus Budrys 1937 metais apkaltintas, kaip tuo
metu buvo įprasta špionažu, Ufoje sušaudytas, kartu su 168
katalikais.[^25] 1958 m. pripažintas nekaltu, o 2003 m. gegužės 31 d.
Romoje pradėtas Beatifikacijos bylos procesas ir kun. Pranciškus Budrys
paskelbtas Dievo Tarnu.[^26]

Mus dominančiu laikotarpiu 1908 - 1919 metais Jekaterinburgo šv. Onos
bažnyčioje klebonavo kunigas Juozas Vilkas. Jauno, energingo kunigo dėka
buvo sukurta labdaros neturtingiems parapijiečiams organizacija,
biblioteka. Prasidėjus I-am pasauliniam karui į Jekaterinburgą ir
apylinkes atvyko evakuotieji ir karo belaisviai, tarp kurių buvo daug
katalikų. I-asis pasaulinis karas pažadino viltį lenkų ir lietuvių
tautiniam atgimimui susigrąžinant valstybingumą. Juozas Vilkas tapo
tautinio patriotinio judėjimo organizatoriumi.­ Jo namai virto štabu net
kelioms organizacijoms: "Lenkų visuomenė", "Karo aukoms", "Harcerų
sąjunga", "Savipagalbos ir informacijos sekcija", buvo įkurta lenkų
mokykla... Dar tebevykstant mūšiams I-jo pasaulinio karo frontuose Urale
kilo pilietinis karas. Per šią suirutę daugelio žmonių likimai prapuolė
į nebūtį. Apie kun. Juozą Vilką žinoma tik tiek, kad jis buvo iškeistas
į kažkokį Lietuvos komunistą.[^27] Duomenų apie šį pasikeitimą nerandu,
tik vienoje Lietuvos parapijoje radau paminėtą kunigą Juozą Vilką, o
būtent Karmėlavos šv. Onos bažnyčioje klebonavusi 1921 - 1927
metais.[^28] Kadangi Lietuvoje pavardė Vilkas nėra reta, tai negaliu
būti tikra, kad tai būtent mus dominantis asmuo.

Jekaterinburgo Romos katalikų internetiniame puslapyje net jo pavardė
įrašyta neteisingai - Josif Vilas, o mano rastuose Vlado ir Jono krikšto
įrašuose: Josif Vilkas ir kun. Pranciškaus Budrio biografijoje taip pat
minimas kun. Juozas Vilkas, o ne Vilas. Ten rašoma, kad kun. Juozas
Vilkas pavadavo kun. P. Budrį, kai tas išvykdavo atostogauti į Lietuvą.
J. Vilkui tam, kad pavaduoti kolegą tekdavo nuvažiuoti net 300 km.

II Pasaulinio karo metais bažnyčios pastate buvo saugomos Ermitažo
vertybės atvežtos iš Leningrado. Vėliau pastate buvo biblioteka, vaikų
darželis, o dar vėliau autobusų stotis. XX a. 7 dešimtmečio pradžioje
pastatas buvo nugriautas nuo tada Malyševo gatvės žiedinėje sankryžoje,
kur kažkada stovėjo didingas bažnyčios pastatas liko tik dykvietė.[^29]
1992 metais katalikų bendruomenė atsikūrė ir dabar pasistatė
priestatą-bažnytėlę netoli tos vietos kur anksčiau stovėjo šv. Onos
bažnyčia.

Kiek artimai Peškaičių šeimyna bendravo su kunigu Juozu Vilku jau nebe
sužinosime, ką tikrai žinome, tai kad jis pakrikštijo kelis Peškaičių
vaikus (Vladą, Joną, tikriausiai Povilą ir Anastasiją), manyčiau ir
mišiose Peškaičiai lankydavosi, jei ne kiekvieną sekmadienį, tai prieš
didžiąsias šventes, tai tikrai. Kadangi kataliku bendruomene šiame
krašte nebuvo labai didelė, manyčiau visi vieni kitus pažynojo.

Mykolas atitarnavęs kariuomenėje jau kartu su žmona Emilija baigė
geležinkeliečių kursus Jekaterinburge. Tuo metu vyko Transsibiro
geležinkelio magistralės vienos iš atšakų statybos ir geležinkeliečių
poreikis buvo išaugęs. Algirdas Peškaitis savo pasakojime mini, kad jo
seneliai dirbo Bilimbajaus geležinkelio aptarnavimo poste, kuris buvo
pastatytas 1909 metais, taigi nuo tų metų Peškaičių šeimyna matyt ten
gyveno ir dirbo.

Bilimbajaus gyvenvietė įkurta 1734 metais, kai buvo atrasti geležies
rūdos telkiniai ir pastatyta ketaus gamykla. Grafų Stroganovų dėka
Bilimbajuje buvo atidarytos vienos pirmųjų Sibire mokyklų ir prie
gamyklos 1820 m. buvo pradėta statyti, vietoje sudegusios medinės,
akmeninė cerkvė. Pagal 1899 m. gyventojų surašymą Bilimbajaus rajone
gyveno 10 000 gyventojų, o 1908 metais gyvenvietėje įvesta elektra.
Įdomu, kad 2014 metais Bilimbajaus gyvenvietės 280-ų metinių progą
geležinkelio stotis vietos gyventoju iniciatyva buvo restauruota ir
stočiai grąžinta pirminė išvaizda. Kaip rašė Rusijos žiniasklaida stotis
nebuvo remontuota nuo pastatymo laikų, o po restauracijos, ji tapo
išskirtine, nes tai vienintelė Transsibiro stotis išsaugojusi savo
pirminę išvaizda.[^30] Tai buvo tipinis IV klasės stoties projektas be
bufeto, kuris buvo statomas ir kitose vietose. Taigi mes galime pamatyti
kaip atrodė Mykolo ir Emilijos Peškaičių darbo vieta. Įdomus dalykas yra
tas, kad Mykolo ir Emilijos Peškaičių socialinis statusas liko tas pats

- Antalieptės valsčiaus valstiečiai. Vadinasi jie taip ir nebuvo
  prisirašę Bilimbajuje, nors ne vienerius metus gyveno Bilimbajuje, o
  vaikus krikštijo Jekaterinburge, artimiausioje katalikų bažnyčioje. Šitą
  faktą patvirtinti kaip ne būtų keistą padėjo kilusi korona viruso
  pandemija 2020 metų pavasarį. Paskelbto karantino metu nemokamą prieigą
  prie savo suskaitmenintų archyvų atidarė Sankt Peterburgo archyvas.
  Būtent tarp šio archyvo dokumentų pavyko surasti Vladislovo ir Jono
  Peškaičių krikšto įrašus Jekaterinburgo Romos katalikų bažnyčios metrikų
  knygose, kurių kolekciją turi Sankt Peterburgo archyvas. O taip pat ir
  Boleslovo Peškaičio gimimo įrašą Petrogrado civilinės metrikacijos
  knygose.

Tokie tipiniai namai buvo statomi prie IV klasės geležinkelio stočių,
tokių kaip Bilimbajaus, gali būti, kad tokiame name ir gyveno Peškaičių
šeimyna:\
![](./media/image45.jpg){width="5.959375546806649in"
height="4.469530839895013in"}

Bilimbajaus stotis pastatyta 1909
metais:![](./media/image26.jpg){width="5.770833333333333in"
height="4.375in"}

restauruota 2014 m. Nuotrauka daryta 2017
balandį:![](./media/image69.jpg){width="6.6930555555555555in"
height="3.8243055555555556in"}

> Bilimbajaus cerkvė pastatyta
> 1820![](./media/image17.jpg){width="6.479166666666667in"
> height="4.375in"}

![](./media/image61.jpg){width="6.479166666666667in" height="4.375in"}

> Bilimbajaus ketaus fabrikas

Nuo kada Peškaičiai gyveno Bilimbajuje neaišku, jeigu remtis
Jekaterinburgo bažnyčios metrikų knygomis, tai jie Bilimbajuje tikrai
gyveno 1912 metais. Tais metais Jekaterinburgo bažnyčioje jie
pakrikštijo Vladą. Kur gimė ir buvo pakrikštyti Antanas ir Magdutė lieka
neaišku. Bilimbajuje Mykolas ir Emilija Peškaičiai, gyveno iki 1917-tų
metų revoliucijos. Po revoliucijos Rusijoje, Lietuvai atgavus
nepriklausomybę (Sovietų Rusiją Lietuvos nepriklausomybę pripažino 1920
m. liepos 12 d.)[^31], jie nutarė grįžti namo, taip pasakoja Algirdas
Peškaitis. Jų kelionė į Lietuvą užtruko kelis metus ir buvo labai sunki
ir sudėtinga. Jie ilgam buvo sulaikyti Petrograde.

Petrograde Peškaičiai vertėsi taip sunkiai, kad net ryžosi du savo
vyresnius sūnūs Antaną ir Vladą atiduoti į vaikų prieglaudą. Spėju, kad
prieglauda buvo prie Romos katalikų šv. Kazimiero bažnyčios, kurį
priklausė lenkų bendruomenei, nes radau paminėtą tik apie šią lenkišką
prieglaudą Petrograde veikusią tuo metu. Iš Algirdo Peškaičio
pasakojimo: „Petrograde mano tėvas Antanas ir dėdė Vladas buvo atiduoti
į vaikų prieglaudą, nes tėvai negalėjo visų vaikų išmaitinti. Ta vaikų
prieglauda buvo lenkiška, kurioje mano tėvas ir dėdė Vladas pramoko
lenkų kalbos, o poterius lenkiškai mokėjo iki pat senatvės."

Šv. Kazimiero bažnyčia prie kurios buvo vaikų prieglauda. Statyta 1989
-- 1908 m., uždaryta 1938 m., išardyta 1956 m., dabar šioje vietoje
stovi telefonų stotis:[^32]

Šv. Kazimiero bažnyčios vidaus
vaizdas:![](./media/image130.jpg){width="3.8031255468066494in"
height="2.946971784776903in"}![](./media/image37.jpg){width="4.761458880139982in"
height="3.3283059930008747in"}

Petrograde 1921 metais Peškaičiams gimė jauniausias sūnus Boleslovas, jo
gimimo įrašą radau Sankt Peterburgo suskaitmenintuose archyvuose. 1921
liepos 26 dieną Petrogrado Kirovo rajono civilinės metrikacijos biure
buvo užregistruotas Baliuko gimimas. Manyčiau, kad Boleslovas buvo
pakrikštytas Romos katalikų šv. Kazimiero bažnyčioje. Į Lietuvą
Peškaičiai grįžo su 6 vaikais: Antanu, Ona, Vladu, Jonu, Povilu ir
Baliu.

Iš viso Mykolas ir Emilija Peškaičiai susilaukė 8 vaikų:

1\. Antanas Peškaitis 1907 - 1977

2\. Ona Peškaitytė Marozienė g. 1909 07 18 Mitraukoje.

3\. Magdalena (Magdutė) Peškaitytė g 1910 m. (mirė vaikystėje).

4\. Vladas Peškaitis g. 1912 09 06 (24 įrašas), (ЦГИА СПб. ф.2292. оп.1.
д.332.)[^33] -- 1989.

5\. Jonas Peškaitis 1913 12 28 (45 įrašas), (ЦГИА СПб. ф.2292. оп.1.
д.351.)[^34]-- 1945 07 01.

6\. Povilas Peškaitis 1915 03 09 -- 2007 12 14.

7\. Anastasija (Nastutė) Peškaitytė 1917 (mirė vaikystėje).

8\. Boleslovas (Balys) Peškaitis 1921 07 12 (937 įrašas), (abecėlinis
sąrašas: ЦГА СПб. ф.Р-6143. оп.2. д.14.; gimimo įrašas: ЦГА СПб.
ф.Р-6143. оп.2. д.277)[^35] -- 1998

Sugrįžę į Lietuvą jie pradžioje apsigyveno Mitraukoje pas gimines
Peškaičius, o vėliau susirado ir išsinuomojo sodybą Baibių kaime.
Mykolas Peškaitis dirbo Kauno geležinkelių stotyje, kaip užrašyta 1925
metais išleistame tarnautojų sąraše, II eilės konduktoriumi[^36], todėl
ir gyveno Kaune, o jo žmona su vaikais liko Baibiuose.

Knygoje "Valstybės įstaigos ir tarnautoja" Mykolo pavardė įrašyta su
klaidom: Pieskaitis, o vardas Mikas, matyt taip ji vadino bendradarbiai:

![](./media/image117.png){width="5.198958880139982in"
height="2.652529527559055in"}

„1922 04 03 priimtas Žemės reformos įstatymas numatė už atlygį nusavinti
dvarų žemes, viršijančias 80 ha. Be atlygio leista nusavinti
tarnavusiųjų Lenkijos kariuomenėje ir bermontininkų būriuose, taip pat
carinės Rusijos valdžios konfiskuotas ir paveldėtojų valdomas žemes.
Žemė po 8--20 ha Lietuvos nepriklausomybės kovų kariams buvo dalijama be
atlygio, kiti turėjo ją išsipirkti per 36 metus. Reforma vyko visą
valstybės egzistavimo laikotarpį, tačiau pagrindiniai pertvarkymai buvo
atlikti iki 4 dešimtmečio pradžios. Reformos metu žemės sklypai paskirti
34 057 bežemiams. Iš viso buvo sudaryti 38 747 naujakurių ūkiai. Žemės
priedus gavo 26 367 mažažemiai. Dar 12 608 sklypai buvo paskirti kaimo
amatininkams, daržams ir sodininkystės ūkiams bei žvejams. Vykdant žemės
reformą, Lietuvoje vyko ir kaimų skirstymas į vienkiemius. Pertvarkytas
Lietuvos žemės ūkis greitai prisitaikė prie Europos rinkos ir jo
produkcija sudarė eksporto pagrindą."[^37]

Vykdant žemės ūkio reforma ir dalijant dvarų žemę Mykolas Peškaitis gavo
10 ha žemės Zokorių kaime iš dvaro, kurio savininku buvo Jonas
Steponavičius.

**Dr. kunigas Jonas Steponavičius.** Dvarininkas dr. kunigas Jonas
Steponavičius gimė Zokorių kaime 1880 kovo 10 dieną dvarininkų Ignoto
Steponavičiaus ir Konstancijos Sakalauskaitės šeimoje. Buvo labai
išsilavinęs žmogus: 1899 m. Baigė Rygos gimnaziją, 1903 m. Vilniaus
kunigų seminariją, 1907 m. -- Peterburgo dvasinę akademiją, 1906 m.
įšventintas kunigu, įgijo teologijos magistro laipsnį. 1910 -- 1912 m.
Miuncheno, Berlyno, Leipcigo universitetuose studijavo psichologiją ir
pedagogiką. 1912 m. įgijo filosofijos daktaro laipsnį[^38].

Jono Steponavičiaus krikšto įrašas Daugailių bažnyčios metrikų knygoje:

![](./media/image128.png){width="5.6885422134733155in"
height="2.2794510061242343in"}

Jono Steponavičiaus mamos Konstancijos Steponavičienės mirties įrašas
Daugailių bažnyčios metrikų
knygoje:![](./media/image89.png){width="6.292708880139982in"
height="2.0344094488188977in"}

1908 balandžio 15 dieną Zokoriuose mirė Konstancija gim. Sakalauskaite
Steponavičienė nuo diabeto 43 metų amžiaus, po savęs paliko vyrą Ignotą
ir sūnų Joną. Palaidota Daugailių kapinėse.

Jono Steponavičiaus tėvo Ignoto Steponavičiaus mirties įrašas Daugailių
bažnyčios metrikų knygoje:

> ![](./media/image38.png){width="5.230208880139982in"
> height="2.330206692913386in"}

1916 metais mirė Ignotas Steponavičius nuo gripo našlys 69 metų amžiaus,
po savęs paliko vienintelį sūnų Joną. Palaidotas Daugailių kapinėse.

Tarpukario metais Lietuvoje Jonas Steponavičius buvo ryškus politikas,
kunigas, filosofijos mokslų daktaras, seimo atstovas. Nuo 1912 m.
paskirtas Vilniaus šv. Jono bažnyčios vikaras. Pirmojo pasaulinio karo
metu dr. kun. J. Steponavičius buvo paskirtas Kaukazo armijos kapelionu.
Aktyviai reiškėsi Tiflisio lietuvių gyvenime, lenkų bažnyčioje aukodavo
Mišias lietuviams, sakė pamokslus, pranešdavo apie įvykius Lietuvoje. Jo
turiningus ir patriotiškus pamokslus akcentavo savo knygoje Stasys
Raštikis. Pasak P. Matekūno, „*Kunigas dr. J. Steponavičius Kaukazo
fronte dalyvavo net pirmosiose kovos linijose, suteikdamas religinius
patarnavimus ne tik sužeistiems lietuvių kariams, bet ir lenkams bei
kitiems, kurie į jį kreipėsi. Jis visiems buvo paslaugus, nuoširdus,
džiaugėsi, galėdamas jiems sunkią valandą padėti, palengvinti kančias.
Fizines kančias ir jam teko pergyventi, nes ir jis pats buvo sužeistas,
gydomas kaip ir visi kariai*"[^39]. 1919 m. grįžęs į Lietuvą klebonavo
Semeliškėse, organizavo savivaldybes Kaišiadorių apskrityje. 1920 m.
lenkų suimtas ir išvežtas į Gardiną. Lenkų fronte dalyvavo kaip
partizanas ir šaulys. 1921 -- 1927 metais steigiamojo, pirmojo, antrojo
ir trečiojo seimo atstovas. Priklausė Lietuvos Krikščionių demokratų
partijos frakcijai. Trečiojo seimo antrasis vicepirmininkas.

kunigas Jonas
Steponavičius![](./media/image6.jpg){width="2.938542213473316in"
height="3.8792629046369203in"}

1927 -- 1933 m. Zarasų gimnazijos direktorius, nuo 1933 m. Utenos
gimnazijos mokytojas. Vokiečių okupacijos metais jis - paskutinis Zarasų
apskrities viršininkas. P. Matekūnas rašo: „*Kunigu dr. J.
Steponavičiumi, kai jį paskyrė Zarasų apskrities viršininku, daug kas
nepasitikėjo, nes žinojo jo simpatija vokiečių tautai ir bijojo, kad jis
„neparsiduotų" vokiečiams. Bet ta baimė greitai dingo, nes visi
pastebėjo, kad jis pasiliko ištikimas savo tautai ir kraštui. Jis
nepataikavo nei vokiečių žemės ūkio atstovui Adleriui, kuris prižiūrėjo
Zarasų apskrityje nacionalizuotus dvarus ir išvežtųjų į Sibirą ūkius,
nei kitiems vokiečiams, o reikalui esant, atvirai, griežtai ir tiesiai į
akis pasakydavo, gindavo savo, kaip apskrities viršininko, teises ar
ūkininkų reikalus dėl pyliavų. Nors jis labai gerai mokėjo vokiečių
kalbą, bet su vokiečiais dažniausiai kalbėjo per vertėjus, nes, kaip yra
sakęs, „okupanto pareiga turėti vertėjus, o užimtojo žmogaus teisė --
visai užmiršti okupanto kalbą"*[^40]*.* Peškaičių giminės santykiai su
Jonų Steponavičiumi buvo labai geri. Gali būti, kad net buvo tolimi
giminės: juk Benedikto Peškaičio sūnus Antanas vedė Sofiją
Steponavičiūtę, dvarininko Romualdo dukterį, kurie taip pat gyveno
Zokoriuose. Būdamas Zarasų apskrities viršininku J. Steponavičius Povilą
Peškaitį paskyrė Degučių valsčiaus viršaičiu, o Joną Peškaitį Degučių
valsčiaus kooperatyvo pirmininku. Algirdas Peškaitis pasakoja: „Aš Joną
Steponavičių gerai prisimenu, nes jis dažnai ateidavo pas tėvą į kalvę
pakaustyti arklį arba ką nors užsakydavo pagaminti. Iki karo J.
Steponavičiaus dvarą valdė ūkvedys Jokūbas Dievaitis, o vėliau Latėnų
didelė šeima su kuria mano tėvai palaikė gerus santykius." Artėjant rusų
kariuomenei Jonas Steponavičius pasitraukė į Žemaitiją. 1944 m. liepos
29 d. Viekšniuose su Žemaitijos kovotojais sudarė viešą aktą įkurti
Tėvynės apsaugos rinktinę.[^41] Rudenį pasitraukė į Vokietiją. Dirbo
Jaegerndorfe apylinkės tekstilės fabrike prie audimo mašinų, tarnavo
paprastu darbininku pas ūkininką Vangeno apylinkėje, paskutiniais
gyvenimo metais darbavosi mechaninėje šaltkalvių dirbtuvėje. Nors jo
gyvenimo sąlygos buvo sunkios, bet, kaip teigia P. Matekūnas, kaip ir
Zarasuose, niekas jo nusiminusio nematė, o priešingai -- gerai
nusiteikęs, gyvas, linksmas, pilnas gražių idėjų ir vilčių ateitimi,
sustiprindamas ne vieną lietuvį tremtinį dvasiškai. Po trumpos ligos
mirė 1947 gruodžio 8 dieną Dr. kun. Jonas Steponavičius palaidotas Šv.
Volfgango katalikų kapinėse Vangene. Lietuviai tremtiniai pastatė kuklų
cementinį paminklą. Dr. Vaclovas Paprovckas straipsnyje „Apie Zarasų
krašto švietėją velionį dr. Joną Steponavičių" rašo, kad jis paliko tai,
kas negenda, nerūdija ir joks priešas nepajėgia atimti, būtent,
dvasinius turtus ir širdį.[^42] Zokorių vienkiemyje išliko dr. Jono
Steponavičiaus gimtasis namas, prie kurio 1996 m. rugsėjo 2 d.
pašventintas tautodailininko J. Tvardausko sukurtas stogastulpis.[^43]

> Bet grįžkime prie pasakojimo apie Mykolo ir Emilijos Peškaičių šeimą.

1828 metais Mykolas ir Emilija Peškaičiai gavo 10 ha žemės Zokoriu
kaime, kaip įdomu ar ne? Nikodemo Peškaičio sūnus praėjus 76 metams
Zokorius. Į klausimą kas dirbo šią žemę, jei Mykolas gyveno Kaune
Algirdas Peškaitis atsakė, kad jo tėvas Antanas žemės nedirbo, jis
turėjo kalvę. Manyčiau vėliau, kai paaugo Antano broliai, taip ir buvo,
bet pradžioje žemę apdirbti turėjo kaip tik Antanas, kuris tuo metu buvo
21 metų amžiaus, o jo jaunesnieji broliai Vladas buvo 16, Jonas - 15 m.,
o jaunėlis Baliukas vos 7 metukų. 1930 metais Antaną pašaukė į
kariuomenę, tarnybos laikas trūko pagal to meto įstatymus -- 18 mėnesių.
Grįžęs iš kariuomenės Antanas Peškaitis ėmėsi kalvio ir staliaus darbų
ir ruošėsi namo statybai Zokoriuose. Kol Antanas buvo kariuomenėje
ūkininkauti Emilijai padėti galėjo jaunesnieji sūnūs Vladas su Jonu,
kurie tuo metu buvo 18 ir 17 metų. Visi kas juodu pažinojo pasakojo, jog
abu mėgo žemę arti. 1931 metų pradžioje ištekėjo Ona Peškaitytė,
vienintelė Mykolo ir Emilijos duktė. Ji ištekėjo už Konstantino Morozo
ir išsikėlė gyventi pas vyrą.

1933-tais metais Emilija su vaikais pradėjo namų statybą Zokorių kaime.
Statybą finansavo Mykolas Peškaitis iš atlyginimo už darbą
geležinkelyje. Gyvenamasis namas buvo pastatytas per du metus. „Aš
gimiau 1934 metų spalio 6 dieną dar Baibių kaime, o sesuo Bronė gimė
1936 metų balandžio 18 dieną jau Zokorių kaime naujame name."[^44]

Mykolas Peškaitis mirė tik pasibaigus karui 1945 metais, jam buvo 64
metai. Emilija Peškaitienė mirė 1960 metais 81 metų amžiaus. Abu
palaidoti Daugailių miestelio kapinėse šalia senelės Emilijos tėvų
Justino ir Anastasijos Kryžinauskų kapo.

Iš kairės viršutinėje eilėje sėdi: Antanas Peškaitis, tarp tėvų
įspraustas Algirdas Brunonas Peškaitis, Anelė Peškaitienė, kuriai ant
kelių sėdi Bronelė Peškaitytė, šalia sėdi Anelės tėvas Vincas Valskys,
antroje eilėje sėdi Boleslovas Peškaitis, Mykolas Peškaitis, Emilija
Peškaitienė ir jos mama Anastasija
Križynauskienė.![](./media/image60.png){width="6.695833333333334in"
height="4.819444444444445in"}

**Algirdo Brunono Peškaičio atsiminimai**

MANO TĖVO ANTANO PEŠKAIČIO ŠEIMA

Mano tėvas Antanas gimė 1907 metais carinės Rusijos Permės gubernijos
Jekaterinburgo mieste (tarybiniais laikais - Sverdlovskas)
geležinkeliečių Mykolo Peškaičio ir Emilijos Kryžanauskaitės šeimoje.
Bilimbajaus miestelyje, kurio geležinkelio stotį aptarnavo tėvai, jis
lankė mokyklą ir iki 1917 metų revoliucijos baigė dvi klases.

Po revoliucijos Rusijoje, kai Lietuva tapo nepriklausoma, Peškaičių
šeima nusprendė grįžti į Lietuvą, šį kelionė trūko labai ilgai, tik 1923
metai jie pasiekė Lietuvą. Pradžioje prisiglaudė pas gimines Mitraukoje,
o vėliau išsinuomojo laisvą sodybą Baibių kaime.

Mano senelis Mykolas gavo darbą Lietuvos geležinkelyje, kadangi darbas
buvo Kaune jam teko palikus šeimą persikraustyti į Kauną. Jo žmona
Emilija su šešiais vaikais liko Baibiuose. Šalia nuomojamos sodybos
gyveno ūkininkas Balys Šileikis, kuris buvo dar ir geras kalvis. Mano
tėvas iš jo mokėsi kalvystės amato, o iš savo senelio Justino
Kryžanausko pramoko staliaus darbų. Kai 1930 metais tėvas buvo pašauktas
į Lietuvos kariuomenę, tai kalvystės ir staliaus amatų mokėjimas jam
pravertė -- jis buvo paskirtas į pionierių batalioną Jonavoje. Pionierių
uždavinys kariuomenėje buvo statyti tiltus, perkėlas ir kitus karinius
objektus. Tarnaudamas kariuomenėje baigė pionierių mokyklą ir įgijo
teisę siekti puskarininkio laipsnio.

Dar 1928 metais vykdant Lietuvoje žemės reformą Mykolo ir Emilijos
Peškaičių šeimai buvo paskirta 10 ha žemės iš Zokorių dvaro, kurio
savininkas buvo Jonas Steponavičius. Grįžęs iš kariuomenės tėvas ėmėsi
kalvio ir staliaus darbų ir ruošėsi namo statybai Zokoriuose.

Tuo metu susipažino su Anele Valskyte, kuri tarnavo Murliškių kaime pas
ūkininką Jurgį Rūgštelę, kurio žmona Marcelė Šileikytė buvo Anelės teta.
Iš pinigų kuriuos užsidirbo Amerikoje Jurgis Rūgštelė buvo nusipirkęs
didelį ūkį su buvusios karčemos pastatais, kurių likučiai ir dabar
matosi šalia plento Utena -- Zarasai.

Ši pažintis baigėsi vestuvėmis, o 1933-ais metais buvo pradėta namų
statyba Zokoriuose. Aš gimiau 1934 metų spalio 6 dieną dar Baibių kaime,
o mano sesuo Bronė gimė 1936 metų balandžio 18 dieną jau Zokorių kaime,
dar po metų 1937 gimė broliukas Vytautas. Gyvenamasis namas buvo
pastatytas per du metus.

1938 metais prikalbinti tėvo draugo Vadeišos visa mūsų šeima išvyko į
Radviliškį, kartų su juo dirbti Linkaičių ginklų fabrike. Vadeiša buvo
tėvo kariuomenės draugas kilęs iš Salako. Linkaičių ginklų fabrike tėvas
tekino artilerijos sviedinius, o mama įsidarbino siuvėja -- siuvo parako
maišelius.

Pradžioje mes gyvenome Radviliškyje Širvintų gatvėje ir su Brone ėjome į
vaikų darželį, kuriame aš nieko nevalgiau. Vėliau persikėlėme į Karčemos
kaimą, kuris buvo arčiau Linkaičių ginklų fabriko ir tėvams nebereikėjo
važiuoti į darbą traukiniu. Šiame kaime nuo plaučių uždegimo mirė mano
broliukas Vytukas. Jis palaidotas Radviliškio kapinėse.

Sovietų Rusijai okupavus Lietuvą Linkaičių ginklų fabrikas dar dirbo, o
1941 metais Vokietijai pradėjus karą prieš Sovietų sąjungą rusai fabriką
susprogdino.

Artėjant vokiečiams visi Karčemos kaimo žmonės susirinko daržinėje, kuri
buvo toliau nuo kaimo. Tėvui nuėjus pažiūrėti buto, jį pasigavo rusų
kareiviai ir vežėsi su savimi, bet užskriejus vokiečių lėktuvams, rusų
kareiviai iššoko į mišką slėptis. Tuo pasinaudodamas tėvas nuo jų pabėgo
ir per dvi savaites pėsčias pasiekė Zokorius. Mes su mama pasilikę
Karčemos kaime, apie tėvo likimą nieko nežinojome, kol pas mus ne
atvažiavo dėdė Balys. Tik iš jo sužinojome, kad tėvas gyvas ir gyvena
Zokoriuose. Grįždamas atgal į Zokorius dėdė Balys pasiėmė ir mane.

1942 metais pradėjau lankyti Baibių pradžios mokyklą prie vokiečių, o
baigiau jau prie rusų 1946 metais. Tėvas dar 1945 metai išvyko gyventi
ir dirbti į Vilnių, nes Zokoriuose gyventi buvo pavojinga. Naktimis
ateidavo „miškiniai", o dienomis kratas darydavo „stribai". Man baigus
pradžios mokyklą tėvas pasiėmė ir manę į Vilnių, kur nuo rugsėjo mėnesio
pradėjau lankyti I-ją berniukų gimnaziją. Šiek tiek vėliau į Vilnių
atsikraustė ir mama su sese Brone, o 1947 metų sausio 23 dieną Vilniuje
gimė dar viena sesė - Staselė.

Mūsų tėvas Antanas dirbo šaltkalviu-derintoju Vilniaus degtinės
gamykloje. Gyvenome gamyklos gyvenamajame name Panerių gatvėje. Mama
buvo namų šeimininkė. Tėvas toje gamykloje dirbdamas nuo 1945 metų iki
1967 metų susigadino sveikatą ir mirė 1977 metai būdamas tik 70 metų
amžiaus. Mūsų mama Anelė gyveno kartu su sesers Stasės šeima ir mirė
1991 metais būdama 76 metų amžiaus. Abu mūsų tėvai palaidoti Vilniaus
Rokantiškių kapinėse.

Norėdamas greičiau būti savarankiškas, dar nebaigęs vidurinės 1950
metais įstojau mokytis į Vilniaus politechnikumą, kurį tais pačiais
metais pervadino į Vilniaus žemės ūkio mechanizacijos ir
elektrifikacijos technikumą, kurį baigiau 1954 metais gaudamas
techniko-elektriko kvalifikaciją. Po baigimo buvau paskirtas į
Vandžiogalos mašinų-traktorių stotį energetiku, bet po mėnesio darbo
šioje stotyje gavau šaukimą į kariuomenę.

Buvau paskirtas į Vilniaus 16-ąją lietuviškąją diviziją. Tarnaudamas
ryšininku-telefonistu šioje divizijoje baigiau Pulko mokyklą ir gavau
seržanto laipsnį. Man be tarnaujant 1956 metais 16-oji lietuviškoji
divizija buvo išformuota ir aš buvau paskirtas į Ventspilyje stovėjusi
pėstininkų pulką. Šiame pulke buvau paskirtas ryšiu kuopos viršila. Po
metų ir šį pulką išformavo, nes jame tarnavo labai daug kaip tada sakė
Pabaltijiečių. Naujas paskyrimas - į Vilnių, tarnybą ir baigiau Šiaurės
miestelyje.

Po tarnybos kariuomenėje aš gyvenau Vilniuje ir dirbau Vilniaus pramonės
gamykloje vyr. energetiku. Į pensiją išėjau 1998 metais iš Geležinio
vilko motorizuotos pėstininkų brigados, joje išdirbęs vyr. energetiku
šešerius metus, jau nepriklausomoje Lietuvoje.

Dar betarnaudamas 1955 metais vedžiau savo kiemo draugę Arimetą
Vojevodskytę (Rimą), su kuria išsiskyrėme 1967 metais. Vaikų neturėjome.
1968 metais vedžiau savo bendradarbę iš Chemijos pramonės valdybos -
Rėdą Berlinskaitę. Su Rėda susilaukėm sūnaus Rėdo gim. 1968 metais
liepos 23 dieną ir dukters Vitos gim. 1970 metais rugsėjo 22 dieną.

Sūnus Rėdas 1992 metais vedė Jolantą Konopliovaitę su kurią vėliau
išsiskyrė, jiedu turi sūnų Steponą Naglį Peškaitį gimusį 1993 metų
lapkričio 28 dieną. Steponas baigė Vilniaus universitetą meteorologiją
ir hidrologiją.

1999 metų balandžio 29 dieną Rėdas vedė Audronę Karpičiūtę, turinčia
dukterį iš pirmosios santuokos Austėją Skaistę Gaigalaitę gimusią 1991
metų gegužės 17 dieną. Rėdas su Audra turi sūnų Marių Algirdą Peškaitį
gimusį 1999 metų liepos 13 dieną. Audronė baigė Vilniaus universitetą
istorijos fakultetą ir dirba Lietuvos nacionalinėje Martyno Mažvydo
bibliotekoje vyr. bibliotekininke. Sūnus Rėdas kompiuterių
programuotojas, informacinių technologijų administratorius ir 13 metų
dirbo spaustuvėje „Spaudos kontūrai" leidinių paruošimo spaudai
specialistu. Austėja gyvena ir dirba Londone menedžere. Marius po
vidurinės baigimo savanoriškai atliko karinę prievolę Kunigaikščio
Vaidoto mechanizuotųjų pėstininkų batalione.

Duktė Vita ištekėjo už Gintaro Žilio, su kuriuo susilaukė dviejų
dukterų: Gerdos Žilytės gimusios 1992 metų gruodžio 20 dieną ir Vėjos
Žilytės gimusios 1997 metų liepos 28 dieną. Vita su Gintaru išsiskyrė,
dabar gyvena ir dirba Londone. Mūsų su Rėda anūkės Gerda ir Vėja taip
pat išvyko pas mamą į Londoną. Gerda 2018 metais baigė Londono
universitete japonų kalbą ir kultūrą, o Vėja baigė koledžą. Ji vizažistė
-- kosmetologė ir dirba vizažiste prancūzų firmos „Chanel" kosmetikos
parduotuvėje.

Mano sesuo Bronė baigė Vilniaus prekybos technikumą ir visą laiką dirbo
Vilniaus miesto maisto parduotuvėse. Buvo ištekėjusi už Algirdo
Augindočio Mikalausko, kuris anksti mirė. Bronė viena užaugino judviejų
dukterį Rūtą Mikalauskaitę, kuri baigė Vilniaus universitetą
psichologiją, turi nuosavą verslą. Rūta ištekėjusi už Sauliaus Zalansko
(g. 1955 02 13), judviejų duktė Dalia Zalanskaitė g. 1979 rugpjūčio 28
dieną taip pat baigė Vilniaus universitetą, filologijos fakultetą,
prancūzų kalbą. Jie visi kartu gyvena Vokietijoje, kur Saulius susirado
darbą.

Sesuo Stasė (Staselė) baigė Vilniaus muzikos mokyklą ir Vilniaus
konservatoriją smuiko specialybę. Daug metų grojo Sauliaus Sondeckio
kameriniame orkestre, vėliau buvo Vilniaus J. Tallat-Kelpšos
konservatorijos mokytoja metodininkė. Šiuo metu dėsto smuiko specialybę
Vilniaus Naujosios Vilnios muzikos mokyklos Styginių instrumentų
skyriaus mokytoja metodininke ir kartu su kolege atlieka šio skyriaus
vedėjos pareigas.

Staselė ištekėjo už Petro Kuncos (g. 1942 02 26), kuris taip pat
smuikininkas ir nuo 1968 metų iki 1999 metų griežė antru smuiku Vilniaus
kvartete, nuo 1974 metų dėstė Lietuvos konservatorijoje, kuri dabar
vadinasi Lietuvos muzikos ir teatro akademija, o nuo 1994 metų šios
akademijos profesorius.

Staselės ir Petro sūnus Simonas Kunca gimęs 1974 metų gruodžio 25 dieną
yra baigęs Vilniaus universitetą. Šiuo metu koncertuoja su grupe
„Hiperbolė Tribute Band" groja fleita ir klavišiniais. Simonas buvo
vedęs Rasą Ladygaitę, bet išsiskyrė, turi dvi dukreles Veroniką Kuncaitę
gimusią 2007 metų birželio 16 dieną ir Agotą Kuncaitę gimusią 2010 metų
balandžio 9 dieną.

TETOS ONOS ŠEIMA

Teta Ona Peškaitytė gimė 1909 metų liepos 18 dieną Mitraukoje,
pakrikštyta Daugailių bažnyčioje, krikštatėviai Justinas Norkūnas su
Uršule Adomo Zakarkos žmona. Seneliai matyt buvo atvažiavę atostogų.
Carinės Rusijos laikais geležinkelio tarnautojai galėjo nemokamai
važiuoti traukiniu visoje carinės Rusijos teritorijoje.

1931 vasario 15 dieną Vajasiškio bažnyčioje dar gyvendama Baibių kaime
ištekėjo už Konstantino Marozo, kuris turėjo 14 ha žemės ūkį Pagirių
kaime už Antalieptės miestelio ir Šventosios upės. Kostas buvo priėmęs
katalikų tikėjimą Lietuvos rusas. Kadangi Ona tekėjo už pasiturinčio
ūkininko, tai gavo atitinkamo dydžio kraitį (pasogą). Kostas buvo
išradingas ūkininkas, verslininkas ir medžiotojas. Ona su savo
sutuoktiniu Kostu susilaukė keturių vaikų:

Mikalina (Mikasė) - gimė 1933 metais

Kostas - gimė 1934 metais

Stanislava (Stasė) - gimė 1935 metais

Adolfas (Adolfukas) - gimė 1942 metais.

Teta Ona su sūnumi Adolfuku 1944-tais metais buvo sužeisti skeveldromis,
sprogusio vokiečių paleisto artilerijos sviedinio, pataikiusio į namą,
kuriame jie tuo metu buvo. Kaimas, kuriame jie buvo apsistoję pas
gimines, jau buvo užimtas sovietinės armijos, todėl sužeistus Oną su
Adolfuku, priėmė į rusų karo lauko ligoninę, o vėliau kartu su
sužeistais sovietinės armijos kareiviais išvežė į Rusijoje esantį miestą
Gus-Khrustalny (Гусь-Хрустальный), esantį už Maskvos 250 km.

Sviedinio skeveldros sužeidusios Oną su sūneliu, nusinešė jos vyro Kosto
gyvybę, bet teta apie tai sužinojo, tik po to, kai grįžo iš Rusijos
namo. Kostas Marozas buvo palaidotas to paties kaimo, kuriame žuvo,
kapinėse.

Marozų namą vokiečių kareiviai sudegino, nes namas trukdė apšaudyti rusų
armijos pozicijas. Sveiki liko, tik tvartas su vištide. Likusiems be
tėvų mano tetos Onos vaikams nebuvo kur gyventi, todėl mano tėvas
Antanas likusiame tvarte sumūrijo krosnį, pagamino ir įstatė langus, o
senelė Emilija iš Zokorių persikraustė į Pagirius prižiūrėti savo
dukters Onos vaikų. Palikti ūkį be priežiūros buvo neįmanoma, juk tokios
suirutės metu ūkį galėjo bet kas užimti ir paskui nieko neįrodysi.

Tuo metu nė dešimties metų neturėjau, bet vis tiek vienas eidavau iš
Zokorių į Pagirius lankyti senelės ir tetos Onos vaikų. Tekdavo eiti per
sudegusius kaimus ir laukus pilnus išraustu apkasų, kuriuose dar vis
mėtėsi šoviniai, granatos, dujokaukės ir kita karinė amunicija. Kad
pasiekčiau savo tikslą man reikėdavo nukeliauti iki Antalieptės,
esančios už 7 kilometrų nuo Zokorių, o nuo Antalieptės iki Pagirių
likdavo dar 5 kilometrai. Už Antalieptės plytinčia giria, mediniu
rastiniu tiltu, per laukus, kuriuose dar gulėjo nužudyti gyvuliai... Tie
vaizdai giliai įsirėžė į mano atmintį. Keista, bet tada man nebuvo baisu
eiti vienam, atvirkščiai aš didžiavausi, kad tėvai manimi taip pasitiki.
Tik dabar pradėjau stebėtis, kaip jie mane dar tokį mažą išleisdavo
vieną.

Neatsimenu kiek laiko praėjo nuo tetos Onos ir pusbrolio Adolfuko
sužeidimų ir jų grįžimo iš Rusijos, bet Adolfuko žaizdos dar nebuvo
sugijusios ir man būdavo baisu matyti jas perrišamas, kai padėdavau jas
apšviesti su žibalinę lempą, savos žaizdos man baimės nekėlė. Adolfukas
pasveiko po sužeidimų ir jau pradėjo eiti į mokyklą, kai 1950 metais
susirgo meningitų ir mirė, jam buvo tik septyneri. Jo sesė mano
pusseserė Mikasė susirgo tuberkulioze dar anksčiau, karo metais, nuo
kurios ir mirė 1959 metais. Abu palaidoti Antalieptės kapinėse.

Pokaryje prasidėjus žemės ūkio kolektyvizacijai, visi kas tik galėjo
bėgo į miestus. Vieni išvyko į Vilnių, kiti -- į Kauną. Abu tetos Onos
vaikai išvyko į Kauną. Mano pusbrolis Kostas -- apsukrus žmogus dar
sovietiniais laikais Raudondvaryje pasistatė didelį namą, kuriame gyvena
jis ir jo dukra Jūratė su šeima. Jo sūnus Saulius Lietuvai atgavus
nepriklausomybę, kaip ir daugelis mūsų tautiečių emigravo ir dabar
gyvena JAV.

Pusseserė Stasė Jankauskienė, giminėje vadinama Kauno Stase, gyvena
dviejų kambarių bute Partizanų gatvėje. Sovietiniais laikais ji dirbo
pramoninės prekybos parduotuvėse. Su pirmuoju vyru Juškevičiumi
susilaukė sūnaus Vytauto (Vytelio), bet santuoka nenusisekė ir Stasė
išsiskyrė. Antrą kartą ištekėjo už Gedimino Jankausko, kurį mes visi
gerai pažinojome ir daug metų bendravome. Gediminą pražudė priklausomybė
nuo alkoholio, todėl ir pusseserė Stasė neapsikentusi išsiskyrė, o jis
likęs vienas gana greitai ir mirė.

Stasės sūnus Vytelis Juškevičius taip pat buvo du kartus vedęs ir turi
du vaikus, po vieną iš kiekvienos santuokos: dukterį Nomedą ir sūnų
Gediminą. Vytautas gyvena savo tėvo, kuris jau mirė, statytame name
kartu su savo dukters iš pirmosios santuokos Nomedos šeima. Nomeda
ištekėjusi turi sūnų Aušrių ir dvi dukrytes, jos brolis Gediminas taip
pat vedęs ir turi dukrą. Tai gi mano pusseserė Stasė jau turi proanūkių.

Onos Peškaitytės krikšto įrašas Daugailių bažnyčios metrikų knygoje.
Šone prierašas apie įvykusią santuoka su Konstantinu Marozu Vajasiškio
bažnyčioje.\
![](./media/image110.jpg){width="6.946527777777778in"
height="2.6458333333333335in"}

DĖDĖS VLADO ŠEIMA:

![](./media/image107.png){width="6.695833333333334in"
height="2.513888888888889in"}

![](./media/image106.png){width="6.695833333333334in"
height="3.0416666666666665in"}

Vladislovas Peškaitis gimė 1912 metų rugsėjo 6 dieną, Permės
geležinkelio Bilimajaus stotyje, pakrikštijo klebonas Juozas Vilkas
Jekaterinburgo bažnyčioje, krikštatėviai Ignotas s. Petro Vilkas su
Eugenija d. Mykolo Šarina mergina.

Po Rusijoje 1917 metais įvykusios revoliucijos visa šeima bandė grįžti į
Lietuvą, bet pakeliui buvo sulaikyti Petrograde. Po revoliucijos
Rusijoje buvo suirutė ir gausiai Peškaičių šeimai buvo sunku išmaitinti
vaikus, todėl tėvai buvo priversti atiduoti du sūnūs Vladą ir Antaną,
tikėdamiesi, kad ten jie bent bus pamaitinti, į lenkišką vaikų
prieglaudą, kurioje jiedu pramoko lenkų kalbos, kuri vėliau Vladui
gyvenime pravertė.

Peškaičiams grįžus į Lietuvą Vladas mokėsi ir baigė Baibių pradinę
mokyklą. Anksti pradėjo dirbti pas ūkininkus, vienas iš jų buvo Zokorių
dvaro savininko Jono Steponavičiaus ūkvedys Dievaitis. Dėdė Vladas
niekada nesiskundė, kad tarnauti buvo sunku ir blogai. Jis visada buvo
geros nuotaikos ir linksmas.

Kartu su mano tėvu statė namus Zokoriuose 1933 metais, kai namą pastatė
iki palangių buvo pašauktas į Lietuvos kariuomenę. Atitarnavęs grįžo
namo į Zokorius, bet neradęs darbo parašė prašymą liktinio tarnybai
kariuomenėje. Jo prašymą patenkinus tarnavo Kėdainių batalione ginklų
remonto dirbtuvėse, gavo puskarininkio laipsnį ir buvo visai patenkintas
kariškio gyvenimu.

Tarnaujant Kėdainiuose Vladas susidraugavo su kitu puskarininkiu iš
Vilkijos miestelio. Jo pakviestas svečiavosi Vilkijoje, kur susipažino
su draugo šeima, o jo sesuo 1938-ais metais tapo Vlado žmona. Kai
1940-ais metais rusai užėmė Lietuvą ir Lietuvos kariuomenė buvo įtraukta
į Sovietinės kariuomenės sudėtį dėdė Vladas buvo perkeltas tarnauti į
Pabradę. Ten pat buvo perkeltas ir dėdė Jonas, kuris taip pat tarnavo
liktiniu kariuomenėje.

Gyvenant Pabradėje, 1941-ais metais gimė dukra Laimutė. Tais pačiais
metais vokiečiams pradėjus karą prieš Rusiją dėdės Vladas ir Jonas
pabėgo iš rusų kariuomenės ir pėsčiomis su mažais vaikai grįžo į
Zokorius. Bet Zokoriuose dėdė Vladas neužsibuvo, kartu su žmona ir
dukrele išvyko į žmonos tėviškę Vilkiją. 1944 m. generolas Plechavičius
per radiją kreipėsi į Lietuvos jaunimą ir pakvietė savanorius į jo
organizuojamą rinktinę. Dėdė Vladas kartu su savo žmonos broliu įstojo į
šią rinktinę, bet ji vokiečių dar tais pačiais metais buvo išformuota.

Vėliau gavo darbą Vilkijos kooperatyve, tapo buhalteriu. Kai 1944-ais
metais rusų armija išvarė iš Lietuvos teritorijos vokiečius ir sukūrė
Lietkoopsąjunga, dėdė Vladas tapo tos sąjungos revizoriumi ir ėmė
važinėti po visą Lietuvą atlikdamas revizijas.

Pirmoji dėdės Vlado žmona buvo uždaro būdo ir nemėgo bendrauti su kitais
žmonėmis, o dėdė atvirkščiai buvo linksmo būdo ir mėgo draugijas, tai
buvo viena iš juos skiriančiu priežasčių, todėl jų skyrybos nieko
nenustebino.

Dirbdamas revizoriumi dėdė lankydavosi ir Kauno Lietkopsąjungos bazėje,
kurioje dirbo prekių žinovė Stefanija, kuriai Vladas labai patiko.
Stefanija sugebėjo sužavėti dėdę Vladą ir jiedu susituokė. Jie abu mėgo
bendrauti su giminėmis ir svečiuotis, todėl daug keliaudavo.

Dėdė Vladas buvo didelis optimistas ir žadėjo gyventi tiek kiek norės,
bet 1992-tais metais susirgo inkstų uždegimu ir greit mirė eidamas 79
metus, jo žmona Stefanija dar ilgai našlavo ir mirė 2012-ais metais. Jie
abu palaidoti Kaune.

Dėdės Vlado duktė mano pusseserė Laima Jakštienė baigusi Vilniaus
universitetą dirbo ekonomiste tarybiniame ūkyje, dabar jau pensininkė ir
gyvena Alytuje. Jos sūnus Martynas Juškus buvo sukūręs šeimą
Vokietijoje, kur susilaukė sūnaus, bet šeimyninis gyvenimas nesusiklostė
ir jo buvusi sutuoktinė su sūnumi liko Vokietijoje, o jis grįžo į
Lietuvą ir taip pat gyvena Alytuje. Dėdė Vladas labai mylėjo savo anūką
Martyną ir kol buvo gyvas visada globojo.

![](./media/image2.jpg){width="1.9379669728783901in"
height="2.9135411198600174in"}![](./media/image23.jpg){width="1.8126192038495188in"
height="2.84375in"}

> Vladas Peškaitis
> 
> ![](./media/image95.png){width="3.3710739282589675in"
> height="4.18437445319335in"}

Vladas ir Stefanija Peškaičiai

DĖDĖS JONO ŠEIMA

![](./media/image139.png){width="6.695833333333334in"
height="2.2083333333333335in"}

![](./media/image137.png){width="6.695833333333334in"
height="2.9722222222222223in"}

Jonas gimė 1913 12 23 Rusijoje, Jekaterinburgo gubernijoje, Bilimbajaus
geležinkelio stotyje, kur geležinkelio stoties poste dirbo jo tėvai. Po
Rusijoje įvykusios revoliucijos su visa Peškaičių šeima grįžo į
nepriklausomą
Lietuvą.![](./media/image85.jpg){width="1.566146106736658in"
height="2.134857830271216in"}

> Pradžioje apsistojo pas gimines Mitraukos kaime, bet tuose namuose
> gyveno labai daug žmonių, todėl Jono tėvai Mykolas su Emilija
> išsinuomojo laisvą sodybą Baibių kaime. Tėvui gavus darbą Lietuvos
> geležinkelyje ir išvykus dirbti į Kauną, kaime liko mama su vaikais.
> Jonas kaip ir kiti vaikai mokėsi Baibių pradžios mokykloje. Ką veikė
> baigęs mokyklą nežinau, greičiausiai tarnavo pas pasiturinčius
> ūkininkus ir mokėsi kokio nors amato.

1934 metais Jonas buvo pašauktas tarnauti į Lietuvos kariuomenę.
Atitarnavęs būtinąją tarnybą pasiliko tarnauti liktiniu ir gavo
puskarininkio laipsnį. Liktiniu tarnavo pulke, kuris buvo dislokuotas
Pabradėje, buvo paskirtas ginklų sandėlio vedėju. Laisvu nuo tarnybos
laiku jis dažnai lankydavosi pas mano tėvus Zokoriuose.

1937 metais sausio 31 dieną Baltriškių bažnyčioje dėdė Jonas vedė
Emiliją Ivanauskaitę, Andriaus Ivanausko ir Salomėjos Jankauskaitės
dukterį, iš Šiukščių kaimo esančio netoli Degučių bažnytkaimio.
Pažinojau Emilijos motiną Salomėją ir du jos brolius Joną ir Lionginą
(Lionių).

Kai Sovietų sąjunga užėmė Lietuvą, tai Lietuvos kariuomenė buvo įjungta
į SSRS kariuomenės sudėtį ir perrengta rusiška uniforma. Šioje
kariuomenėje dėdė Jonas su dėdė Vladu tarnavo iki 1941 metų karo
pradžios. Vokiečiams užimant Lietuvą, rusų kariuomenė traukėsi į Rusiją,
bet nei dėdė Jonas, nei dėdė Vladas likti rusų armijoje nenorėjo. Todėl
jie savavališkai iš jos pasitraukė ir su šeimomis pėsčiomis pasiekė
Zokorius. Tada dėdė Jonas ant rankų parsinešė mažutį sūnų Meldutį gimusi
1940 kovo 27 dieną.

Užėmę Lietuvą vokiečiai Zarasų apskrities viršininkų paskyrė Joną
Steponavičių, kuris gerai mokėjo vokiečių kalbą, nes buvo baigęs
aukštuosius mokslus Vokietijoje. Steponavičius gerai pažinojo visus
Peškaičius, juos gerbė ir jais pasitikėjo, todėl dėdę Povilą paskyrė
Degučių valsčiaus viršaičiu, o dėdę Joną to valsčiaus kooperatyvo
pirmininku. Prie vokiečių dėdė Jonas su šeima gyveno Zokoriuose, namo
salikoje (antrame aukšte), ten gyvendami 1942 metų liepos 17 dieną
susilaukė dukters Birutės, o 1943 gruodžio 8 dieną gimė dar vienus sūnus
Mindaugas.

Dėdė Jonas buvo labai gabus žmogus. Dar tais laikais jis įrengė vėjo
elektros stotelę ant namo stogo, o salikoje sumontavo akumuliatorių su
valdymo pultu. Jau tada mūsų namai buvo elektrifikuoti. Elektra buvo
nuvesta ir kaimynui Povilui Maračinskui. Tuo metu aš ir sužinojau kas
yra elektra ir ką ji gali nuveikti.

1944 metais vokiečių kariuomenei traukiantis stumiamai rusų armijos
dėdės Povilas su Jonu nusprendė, kad jiems saugiau bus pasitraukti į
Vokietiją. Povilui su šeima pavyko pasitraukti ir patekti į vėliau
amerikiečių užimamą teritoriją, o dėdės Jono šeimai nepasisekė. Jonui su
šeima besitraukiant juos pasivijo rusų armija ir jie buvo priversti
grįžti į Zokorius.

Rusams užėmus Lietuvą visi jauni vyrai buvo imami į kariuomenę ir
siunčiami į frontą kariauti su vokiečiais. Dėdė Jonas nenorėdamas
kariauti prieš vokiečius į Tarybinę kariuomenę nestojo ir pasislėpė
paties įsirengtoje slėptuvėje, kuri buvo įrengta rūsyje po bulvių
aruodų. Apie tą slėptuvę aš žinojau. Ilgai gyventi slėptuvėje buvo
pavojinga ir kenksminga sveikatai, todėl mano tėvas atidavė savo karinį
bilietą savo broliui Jonui. Gerai prisimenu tą vakarą, kai kariniame
biliete buvo taisomas vardas iš Antano į Jono. Su tuo kariniu bilietu
dėdė Jonas gavo pasą ir išvažiavo į Kauną, kur gavo darbą pieninėje, bet
jo šeima dar buvo likusi Zokoriuose. Norėdamas aplankyti šeimą ir jei
pavyks pasiimti į Kauną dėdė Jonas su pakeleivinga mašina atvažiavo iki
Baibių. Jam išlipus iš mašinos jį pamatė rusas iš Daneikių kaimo ir tuoj
pat apie tai pranešė stribams, kurie greitai atvyko į Zokorius. Dėdė
Jonui pavyko nuo stribų pabėgti ir pasislėpti pas pusbrolius Valiukėnus
Mikolojūnų kaime. Pusbroliai įkalbėjo nebegrįžti į Kauną, bet eiti į
mišką pas miško brolius, kurie buvo įsikūrę Šlepečių kaimo miške. Tada
visi tikėjosi, kad Amerika greitai išvaduos Lietuvą iš rusų okupacijos.
Dėdė Jonas tikėdamas tais pažadais ir nuėjo į mišką partizanauti.

Maždaug po mėnesio, kažkam išdavus partizanų buvimo vietą, rusų
kariuomenė kartu su stribais apsupo mišką, kuriame slėpėsi partizanai ir
prasidėjo kautynės. Tai įvyko 1945 metų liepos 1 dieną apie 3 valandą
dienos. Tuo metu mes su kaimo paaugliais buvome prie Šekšio ežero ir
ruošėmės granatomis sprogdinti žuvį, bet nespėjom. Išgirdome šūvius ir
sprogimus\...

Vėliau sužinojome, kad partizanai traukėsi, o jų pasitraukimą dengė dėdė
Jonas su draugu. Visi partizanai pasitraukė, o dėdė Jonas su draugu buvo
sužeisti ir apsupti. Nenorėdami pasiduoti rusų kareiviams susisprogdino
granatomis.

Rusų kareiviai nesugavę partizanų, vienoje Šlepečių kaimo troboje atradę
keturis kaimo vyrus žaidžiančius kortomis, juos sumušė ir nušovė. Šalia
tos trobos suguldė ir tuos keturis vyrus ir miške žuvusius partizanus.
Močiutė Emilija ėjo pažiūrėti ar tikrai ten jos sūnus Jonas.
Neįsivaizduoju ką ji jautė žiūrėdama į savo žuvusį sūnų, kurio ir gedėti
negalėjo, nes jei rusai būtų pamatę ją verkiančią būtų pačiupę ir išvežę
į Sibirą ne tik ją, bet ir visą šeimą. Tik po trijų dienų stribai leido
juos palaidoti Šlepečių kaimo kapinėse, kurios buvo tik kitoje kelio
pusėje priešais namą, šalia kurio jie buvo suguldyti. Tik atgavus
nepriklausomybę, vienos mokytojos iniciatyva, tose kaimo kapinėse buvo
pastatytas paminklas su žuvusiųjų vardais. Aš dalyvavau kaimo kapinių
tvarkyme ir pašventinimo iškilmėse. Į iškilmes atvyko ir dėdės Jono
vaikai Meldutis Jonas, Birutė ir Mindaugas.

Po dėdės Jono žūties, jo žmona Emilija su vaikais išvyko į Kauną, nes
bijojo, kad gali būti ištremta į Sibirą. Jonienė Kaune gyveno sunkiai,
dirbo kiemsarge, vėliau Kauno filharmonijoje rūbininke. Vienai auginti
tris vaikus be galo sunku ir geresniais laikais.

Meldas vedė Nijolę Tamulaitytę gimusią 1943 metų gegužės 17 dieną ir
gyveno Kaune, Aleksote uošvių name, dirbo taksistu. Meldo uošvis Jonas
Tamulaitis nepriklausomybės laikais buvo VSD karininkas dirbo A.
Smetonos apsauginiu ir asmeniniu vairuotoju, todėl 1944 metais
pasitraukė į Vakarus, kaip tada galvojo trumpam. Jo žmona Zofija
Tamulaitienė dėl to buvo tardoma 1949 metai bandant sužinoti kur jos
vyras, bet tuo metu ji ir pati nieko nežinojo. Vėliau po Stalino mirties
vyras atsiuntė laišką, kad jis ir jų vyriausias sūnus Alfredas gyvena
Kanadoje. Jiedu taip ir nesusitiko.

Tamulaičių name kartu gyveno seserų Nijolės ir Jūratės šeimos. Meldutis
Jonas Peškaitis su Nijole susilaukė dviejų vaikų: Arūno Peškaičio
gimusio 1962 metų kovo 6 dieną ir dukters Eglės gimusios 1971 rugsėjo 5
dieną.

Arūnas Peškaitis dėl silpnos sveikatos pasirinko studijuoti psichologiją
Vilniaus universitete. Studijas baigė, bet pagal specialybę nedirbo.
Šiuo metu jis spaudoje apibūdinamas taip: psichologas, kunigas,
religijotyrininkas, publicistas, vienuolis pranciškonas, Bernardinų
parapijos vikaras, nuteistųjų iki gyvos galvos kalinių kapelionas.

Eglė Peškaitytė baigė Kauno paslaugų verslo darbuotojų profesinio
rengimo centrą, smulkių gyvūnų prižiūrėtojo specialybę ir dirba VŠĮ
„Lesė" Kauno skyriuje. 1991 metais ištekėjo už Kęstučio Baleženio. Turi
keturis vaikus: dukras Justę ir Kastutę, sūnūs Kęstutį ir Justą Vakarį.
Kastutė su vyru Dariumi turi sūnų Matą.

Dėdės Jono Peškaičio duktė Birutė gyvena Kaune su antruoju vyru Antanu
Aliumi Jurkevičiumi. Iš pirmos santuokos su Algiu Baltuška turi dukrą
Jolantą gimusią 1964 metų vasario 21 dieną. Jolanta Baltuškaitė
Strumilienė ištekėjusi trečią kartą už Vitalijaus Strumilo, pirmas vyras
Kęstutis Lukoševičius, antras Virginijus Švarcas. Turi dvi dukras Vaidą
Lukoševičiūtę gimė 1983 metais ir Donatą Švarcaitę gimė 1991 metais.
Vaida Lukoševičiūtė ištekėjo už Ramūno Lukoševičaius ir susilaukė sūnaus
Luko, o pirmai santuokai iširus su Aliumi Bučinsku susilaukė dukters
Emilijos. Vaida su vaikais šiuo metu gyvena užsienyje.

Jauniausias Jono Peškaičio sūnus Mindaugas su žmona Gražina susilaukė
sūnaus Ramūno ir dukters Rasos, ji ištekėjo už Artūro Simonavičiaus ir
gyvena Karmėlavoje.

**Jono Peškaičio dukters Birutės atsiminimai**

Kiek žinau iš mamytės pasakojimų, po tėvelio žūties turėjome bėgti
slapstytis kad nebūtumėm išvežti į Sibirą arba dar blogiau, kaip bandito
šeima, sušaudyti. Apsigyvenom Kaune pas tėvelio pusbroli Valiukėna. Buvo
labai sunku vienam kambary gyvenom 10 žmonių, trūko visko, dažnai
badavom. Su mumis dar buvo mamytės mama ji sirgo sunkia astmos forma,
gulėjo 10 metų lovoje. Ji mirė 1957 m.Vyriausias Meldutis-Jonas liko
kaime pas dėdę Balį, nes jis jau galėjo padėti jiems ūkyje, ganyti avis
ar šiaip prie smulkių darbų. Ten jis baige 4 skyrius ir mama pasiėmė jį
į Kauną. Labai sunkiai vertemės, mamyte dirbo gatviu valytoja, tai mes
eidavom anksti ryte padėti jai valyti gatves, nes budavo žiemą labai
daug sniego, reikėdavo ta sniegą nukasti ir išvežti į Nemuną, o po to
mes eidavom į mokyklą. Nesinori prisiminti ką teko iškęsti. Meldas
baigęs vidurinę mokėsi politechnikume, bet neteko baigti, buvo paimtas į
armiją. 1961 jis vede Nijole Tamulaityte, 1962 gime sūnus Arūnas, 1971
gime dukra Eglė.

Meldutis gimė 1940 03 27 Aš gimiau 1942 07 17 baigiau vidurine, tiesa iš
10 klasės turėjau išeiti į vakarinę, nes pradėjau dirbti, reikėjo padėti
mamai išlaikyti šeimą, bet man tai buvo geras pretekstas ne stoti į
komjaunimą, kadangi atnešė visiems anketas ir turėjom visi užpildyti, o
aš išėjau iš mokyklos ir taip išsisukau nuo komjaunimo. Po vidurinės
baigiau prekybos mokyklą, tais laikais būti pardavėja buvo prestižas,
tada jau pasidar lengviau gyventi, padedavau mamai,jau buvom gave
valdiska vieno kambariuka su betoninem grindim be jokiu patogumu bet
vistiek jau vieni. As 1962 istekejau uz Baltuskos
Bronislovo-Algimanto.1974-santuoka nutrūko. 1977 istekejau antra karta
uz Jurkeviciaus antano-Aloyzo jis mire 2013.Su Baltuska susilaukiau
dukros Jolantos-Margaritos.Mindaugas gime 1943 m. 1963 vede Grazina
Rinkeviciute, 1964 gime dukra Rasa, 1968-sunus Ramunas.1977 Grazina
mirė. Mindaugas mirė 1993 m. Meldas mirė 2007 vasario mėn, o jo žmona
Nijole 2007 rugseji.

Jų buvo 3 vaikai ir jie du tai jau penki.O mūsų irgi penki.Valiūkė nų
atsi.eni dukrytės vardas buvo Daivutė vieno jaunoausio sūnelio vardas
Antanukas o vyriausio lygtai Romas.Jų mama anksti mirė atrodo kad
29m.liko labai maži vaikai.Mes išsikėlėm pas kažkokius pažystamus ar
gimines nežinau tik atsimenu kad pavardė buvo Mikličius.Ten buvo dsugiau
vietos, turėjom atskirą kambarį

Jono Peškaičio ir Emilijos Ivanauskaitės priešsantuokinės apklausos
anketa:

![](./media/image53.jpg){width="6.6930555555555555in"
height="10.29861111111111in"}

![](./media/image86.jpg){width="6.090277777777778in"
height="3.7895833333333333in"}

![](./media/image79.jpg){width="6.135416666666667in"
height="3.63125in"}

Meldučio Jono Peškaičio ir Nijolės Tamulaitytės vestuvės

![](./media/image54.png){width="2.859027777777778in"
height="4.919444444444444in"}![](./media/image116.png){width="2.8520833333333333in"
height="4.683333333333334in"}

Nijolė Tamulaitytė ir Meldutis Jonas Peškaitis 1962 m.

Jonas ir Zofija Tamulaičiai, Kaunas 1939 m.

Meldas Peškaitis, Arūnas Peškaitis ir Nijolė Peškaitienė\
![](./media/image74.jpg){width="6.59532261592301in" height="4.6875in"}

![](./media/image52.jpg){width="3.9260958005249345in"
height="3.5385411198600174in"}

Kun. Arūnas
Peškaitis.![](./media/image75.jpg){width="3.6629451006124234in"
height="4.923957786526684in"}

Eglė Baležentienė

![](./media/image43.jpg){width="5.2875in" height="5.2875in"}

> Eglė Peškaitytė ir Kęstutis Baležentis švenčia savo 25-tas vestuvių
> metines 2016 m.![](./media/image104.jpg){width="3.276388888888889in"
> height="3.276388888888889in"}

Justė Baležentytė

![](./media/image44.jpg){width="3.0218755468066494in"
height="5.371482939632546in"}

Kastutė Baležentytė ir Darius Kregždanas

Kęstutis Baležentis su sūnumi
Kęstučiu![](./media/image11.jpg){width="4.480208880139982in"
height="4.547944006999125in"}

Birutė Jurkevičienė su proanūkiais Luku Lukoševičiumi ir Emilija
Bučinskaite 2014
metais![](./media/image28.jpg){width="6.477083333333334in"
height="9.715972222222222in"}

Jolanta Baltuškaitė Lukoševičienė Švarcienė Strumilienė su anūkais 2014
metais.![](./media/image47.jpg){width="6.257638888888889in"
height="9.386111111111111in"}

Vaida Lukoševičienė su vaikais Luku Lukoševičiumi ir Emilija Bučinskaite
2019 m.

![](./media/image119.jpg){width="5.865625546806649in"
height="7.817793088363954in"}

Donata Švarcaitė 2017
m.![](./media/image111.jpg){width="6.083333333333333in"
height="6.054950787401575in"}

![](./media/image123.png){width="6.083333333333333in"
height="4.491527777777778in"}

DĖDĖS POVILO ŠEIMA

Dėdė Povilas, kaip ir dauguma Peškaičių vaikų gimė 1915 metais Rusijoje
Jekaterinburgo gubernijoje, Bilimbajaus miestelyje, kur geležinkelio
stoties poste dirbo jo tėvai. Šeimai sugrįžus į Lietuvą baigė pradžios
mokyklą ir kirpėjų bei fotografų kursus. Po baigimo nuo 1938 metų gyveno
ir dirbo kirpėju ir fotografu Saldutiškyje savo kirpykloje-fotoateljė.
1941 metais prasidėjus karui grįžo į Zokorius. Vokiečių okupacijos
metais Zarasų apskrities viršininku dirbo Steponavičius Jonas Zokorių
ūkio savininkas, kuris dėdę Povilą paskyrė Degučių valsčiaus viršaičiu,
o dėdę Joną Degučių kooperatyvo pirmininku.

Tuo metu dėdė Povilas susipažino su Stefanija Simonavičiūtė
(Samanavičiūtė), gimusia 1920 metais, iš Lundiškių kaimo Antalieptės
valsčiaus ir ją vedė. Povilas su žmona Stefanija 1942 metais susilaukė
sūnaus Stasiuko.

1944 metais, artėjant rusų kariuomenei, dėdė Povilas su šeima traukėsi
iš Lietuvos kartu su vokiečių kariuomene. Jiems pavyko pasitraukti į
Vokietiją, amerikiečių užimtą zoną, kur buvo apgyvendinti pabėgėlių
stovykloje. Stovykloje gyvendami 1946 metais susilaukė dukters Vandutės.
Vokietijoje pragyveno iki 1950 metų. Išsirūpinus iškvietimą į Ameriką,
kurį atsiuntė Jonas Peškaitis, antros eilės pusbrolis, jo dėdės
Benedikto sūnus, Povilas su šeimą persikėlė gyventi į JAV. Jonas nuo
1928 metų gyveno Čikagoje, ten gyveno ir jo sesuo Leokadija ištekėjusi
už uteniškio Šaltenio.

Iš pradžių Povilo šeimai gyvenimas svetimoje šalyje buvo sunkus ir
atrodė, kad nebeliko jokių vilčių, vienu metu net kilo mintis, kad
vienintelė išeitis, tai visos šeimos savižudybė. Bet Povilui įsidarbinus
gamykloje tekintoju, gyvenimas palengvėjo, o su laiku dėdės Povilo šeima
prakuto, Čikagoje įsigijo nuosavus namus, kuriuose Povilas su šeima
gyveno iki žmonos Stefanijos mirties. Tekintoju dėdė dirbo iki 63 metų
amžiaus, ištikus infarktui ir atlikus operaciją teko išeiti į išankstinę
pensiją, nes dirbti jau negalėjo. 1980 metais mirus Stefanijai, kad
neapsunkinti vaikams gyvenimo Povilas persikraustė į lietuvių globos
namus Lemonte netoli Čikagos. Ten ir pragyveno iki pat savo mirties 2007
metų gruodžio 14 dieną sulaukęs 92 metų amžiaus.

Povilo ir Stefanijos duktė Vanda ištekėjo už lietuvio Alfonso Rauckino,
jiedu turi dvi dukras Rasą ir Audrę ir sūnų Paulių.

Audre Rauckinas yra ištekėjusi už Tomo Pociaus ir jiedu turi tris dukras
Stephanie Pocius, Alissa Pocius ir Sophia Pocius. Audre dirba Čikagos
šv. Juozapo ligoninėje, ji neseniai baigė šv. Pranciškaus universitetą
Džolieto mieste, Ilinojaus valstijoje JAV, kuris yra už 56 km. nuo
Čikagos ir gavo magistro laipsnį. Rasa Rauckinas ištekėjusi už Raimio
Kelpšos ir turi dvi dukras Kristina Kelpša ir Daina Kelpša. Raulius
Rauckinas vedęs Lisa Macy ir turi sūnų Alex Rauckinas. Mano pusseserė
Vandą turi penkias anūkes ir vieną anūką. 2017 metų vasarą Vanda su vyru
ir savo trimis vaikais buvo atvažiavę į Lietuvą.

Povilo sūnus Stasys Peškaitis vedė anglų kilmės amerikietę Patricia
Rochkes, jiedu turi dukrą Pamelą ir sūnų Stepą (Stivą). Patricia
ištekėjusi už Anthony Philip Perino ir turi sūnų Anthony Perino.

![](./media/image98.jpg){width="1.9395833333333334in"
height="3.2805555555555554in"}

Povilas Peškaitis

![](./media/image57.jpg){width="5.490277777777778in"
height="3.582638888888889in"}

Povilas Peškaitis, Vanda Peškaitytė, Stasys Peškaitis ir Stefanija
Peškaitienė

Registracijos kortelė užpildyta Povilo Peškaičio
ranka.![](./media/image149.jpg){width="6.6930555555555555in"
height="4.238888888888889in"}

Pabėgėlių sąrašas, kuriame yra Peškaičių šeima ir nurodytas asmuo, kuris
juos priims, čia nurodyta Petronėlė Peškaitienė ir jos adresas
Čikagoje.![](./media/image131.png){width="6.6930555555555555in"
height="0.7354166666666667in"}

![](./media/image100.jpg){width="6.6930555555555555in"
height="5.020138888888889in"}

Rauckinai iš kairės stovi:

Raimis Kelpša, Rasa Rauckinas Kelpša, Tom Pocius, Vanda Peskaitis
Rauckinas, Audre Rauckinas Pocius, Adolfas Rauckinas, Lisa Macy
Rauckinas, Paulius Rauckinas.

Tupi priešais: Kristina Kelpša, Daina Kelpša, Alissa Pocius,

Sophia Pocius, Stephany Pocius,

Alex Rauckinas.

Iš atrasto Boleslovo Peškaičio gimimo įrašo civilinės metrikacijos
skyriuje sužinome, kad Peškaičių šeima Petrograde gyveno dabartiniame
Kirovo rajone, kuriame buvo lenkų bendruomenės šv. Kazimiero bažnyčia,
gali būti, kad jis buvo pakrikštytas būtent šioje bažnyčioje. Šis faktas
netiesiogiai patvirtina, kad Baliuko vyresnieji broliai Antanas ir
Vladas buvo atiduoti į būtent prie šios bažnyčios esančią prieglaudą.

![](./media/image88.png){width="6.053125546806649in"
height="3.5772747156605424in"}![](./media/image70.png){width="6.0635422134733155in"
height="2.753583770778653in"}

Boleslovo Peškaičio ir Henrikos Bružaitės priešsantuokinės apklausos
anketa:

![](./media/image55.jpg){width="6.6930555555555555in"
height="9.061805555555555in"}

[^1]: Lietuvos inventoriai XVII a. = Инвентари Литвы XVII в. : dokumentų
    rinkinys / Lietuvos TSR mokslų akademija. Istorijos institutas ;
    sudarė K. Jablonskis ir M. Jučas, Vilnius, 1962

[^2]: http://www.dusetukrastas.info/dusetu-krastas-6/

[^3]: Lenkų-Lietuvių kalbų žodynas

[^4]: https://www.paneveziovyskupija.lt/parapijos/daugailiu-sv-antano-paduviecio-parapija/

[^5]: https://www.facebook.com/604946983335911/photos/a.605041169993159/934701597027113/

[^6]: https://www.genmetrika.eu/daugailiu_metrikai1.html

[^7]: https://sena.pavb.lt/lt/straipsnis/3048-zarasu-kraste

[^8]: sodyba už gatvinio kaimo: Už gatvinių kaimų išsikėlusios sodybos
    buvo vadinamos užusieniais. Kai kurie kaimai ir ypač užusieniai buvo
    įsiterpę tarp kitų dvarų žemių. Žodynas.lt

[^9]: Vytautas Indrašius, Antalieptės kraštas: vietovių istorinės
    apybraižos. -- Vilnius: LNRS, 2008--2009. I T. A -- M. 2008. 453
    psl.

[^10]: Familysearch.org

[^11]: Leonas Mulevičius „Kaimas ir dvaras Lietuvoje XIX amžiuje",
    Vilnius 2003, 60 -- 65 psl.

[^12]:  Ten pat, 291-292 psl.

[^13]: Leonas Mulevičius „Kaimas ir dvaras Lietuvoje XIX amžiuje",
    Vilnius 2003, 84 psl.

[^14]: Ten pat, 84 psl.

[^15]: Ten pat, 453 psl.

[^16]: Ten pat, 455 psl.

[^17]: http://lists.memo.ru/index16.htm

[^18]: https://ru.wikipedia.org/wiki/%D0%92%D0%BE%D0%B5%D0%BD%D0%BD%D0%B0%D1%8F\_%D1%81%D0%BB%D1%83%D0%B6%D0%B1%D0%B0

[^19]: https://ria1914.info/index.php/222-%D0%B9\_%D0%B7%D0%B0%D0%BF%D0%B0%D1%81%D0%BD%D1%8B%D0%B9\_%D0%BF%D0%B5%D1%85%D0%BE%D1%82%D0%BD%D1%8B%D0%B9\_%D0%B1%D0%B0%D1%82%D0%B0%D0%BB%D1%8C%D0%BE%D0%BD

[^20]: http://otpusk-info.ru/journey/encyclopedia/ekaterinburg/articles/9/garnizon.htm

[^21]: https://ria1914.info/index.php/%D0%95%D0%BA%D0%B0%D1%82%D0%B5%D1%80%D0%B8%D0%BD%D0%B1%D1%83%D1%80%D0%B3

[^22]: https://www.vle.lt/Straipsnis/Jekaterinburg-34494

[^23]: http://www.ekatcatholic.ru/hrono/albums/oldphotos.php

[^24]: https://www.e1.ru/news/spool/news_id-51011731.html

[^25]: http://www.ekatcatholic.ru/history.php

[^26]: https://lt.wikipedia.org/wiki/Pranci%C5%A1kus_Budrys

[^27]: Т.П. Мосунова, Духовенство Екатеринбургского католического
    прихода святой Анны в ХІХ- первой половине XX в., 137 с.

[^28]: [[http://karmelavosparapija.lt/9-parapija]{.ul}](http://karmelavosparapija.lt/9-parapija)

[^29]: https://www.e1.ru/news/spool/news_id-51011731.html

[^30]: [[https://www.oblgazeta.ru/zemstva/21400/]{.ul}](https://www.oblgazeta.ru/zemstva/21400/),
    [https://ru.wikipedia.org//wiki/Билимбай\_(станция)]{.ul}

[^31]: https://lt.wikipedia.org/wiki/Lietuvos_Nepriklausomyb%C4%97s_Aktas

[^32]: https://www.citywalls.ru/house9453.html

[^33]: [[https://spbarchives.ru/infres/-/archive/cgia/2292/1/332]{.ul}](https://spbarchives.ru/infres/-/archive/cgia/2292/1/332),
    9 (7) psl.

[^34]: [[https://spbarchives.ru/infres/-/archive/cgia/2292/1/351]{.ul}](https://spbarchives.ru/infres/-/archive/cgia/2292/1/351),
    15 (13) psl.

[^35]: [[https://spbarchives.ru/infres/-/archive/cga/R-6143/2/299]{.ul}](https://spbarchives.ru/infres/-/archive/cga/R-6143/2/299),
    142 psl (132) psl.; ,
    [[https://spbarchives.ru/infres/-/archive/cga/R-6143/2/277]{.ul}](https://spbarchives.ru/infres/-/archive/cga/R-6143/2/277)
    94 (90) psl.

[^36]: Valstybės įstaigos ir tarnautojai. - 1925, Kaunas, 90 psl.

[^37]: http://www.istorineprezidentura.lt/balsavimas/rezultatai.php?visi=&rodyti=irasa&irasas=134

[^38]: https://lt.wikipedia.org/wiki/Jonas_Steponavi%C4%8Dius

[^39]: http://www.voruta.lt/dr-kun-jonas-steponavicius-1880-03-10-1906-1947-12-08-kovotojas-del-lietuvybes-vilnijos-kraste/

[^40]: http://www.voruta.lt/dr-kun-jonas-steponavicius-1880-03-10-1906-1947-12-08-kovotojas-del-lietuvybes-vilnijos-kraste/

[^41]: https://lt.wikipedia.org/wiki/Jonas_Steponavi%C4%8Dius

[^42]: http://www.voruta.lt/dr-kun-jonas-steponavicius-1880-03-10-1906-1947-12-08-kovotojas-del-lietuvybes-vilnijos-kraste/

[^43]: https://lt.wikipedia.org/wiki/Jonas_Steponavi%C4%8Dius

[^44]: Iš Algirdo Peškaičio pasakojimo.
